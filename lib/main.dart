import 'package:firebase_core/firebase_core.dart';

import 'src/middleware/sesionMiddleware.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:google_map_location_picker/generated/l10n.dart'as location_picker;
import 'package:google_map_location_picker/generated/l10n.dart';


void main() => runApp(MyApp());//metodo raiz, el que lanza la aplicación

class MyApp extends StatelessWidget {//la clase myApp extiende al widget StatelessWidget que contiene la vista, es decir,
  // el arbol de widget de la app




  @override
  Widget build(BuildContext context) {
    return MaterialApp( //se retorna una vista con material design
        title: 'WhereApp',
        theme: ThemeData(
          primaryColor: Color(0xff977EF2),
          accentColor: Color(0xff977EF2),
        ),
        debugShowCheckedModeBanner: false,
        localizationsDelegates: const [
          location_picker.S.delegate,
          S.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: const <Locale>[
          Locale('en', ''),
          Locale('ar', ''),
          Locale('pt', ''),
          Locale('tr', ''),
          Locale('es', ''),
          Locale('it', ''),
          Locale('ru', ''),
        ],
        home: sesionMiddleware() //en el home de la aplicacion se muestra lo que está consignado en la clase sesionMiddleware()
    );
  }
}
