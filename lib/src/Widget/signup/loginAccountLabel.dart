import 'package:flutter/material.dart';

import 'package:whereappv2/src/utils/TextStyles.dart';

import '../../loginPage.dart';

class loginAccountLabel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20),
      alignment: Alignment.bottomCenter,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          NormalText("¿Ya tienes cuenta?",Color(0xff000000), 15),
          SizedBox(
            width: 10,
          ),
          InkWell(
            onTap: () {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => LoginPage(false)));
            },
            child: NormalText("¡Iniciar sesión!",Color(0xff1A44F1), 15),
          )
        ],
      ),
    );
  }
}