import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';

class showProgressDialog{
  ProgressDialog pr;

  showDialog(BuildContext contexto, String msj)async{
    pr = new ProgressDialog(contexto);
    pr = ProgressDialog(contexto,type: ProgressDialogType.Normal, isDismissible: true, showLogs: false);
    pr.style(
        message: msj,
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );
    await pr.show();
  }

  updateShowDialog(String msj)async{

      pr.update(
        progress: 50.0,
        message: msj,
        progressWidget: Container(
            padding: EdgeInsets.all(8.0), child: CircularProgressIndicator()),
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 17.0, fontWeight: FontWeight.w600),
      );

  }

  hideShowDialog()async{
    await pr.hide();
  }

}