import 'package:flutter/material.dart';
import 'package:whereappv2/src/utils/TextStyles.dart';

class backButton extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return InkWell(
      onTap: () {
        Navigator.pop(context);
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 0, top: 10, bottom: 10),
              child: Icon(Icons.arrow_back , color: Colors.black),
            ),
            NormalText("Atrás",Color(0xff000000), 17),
          ],
        ),
      ),
    );
  }

}