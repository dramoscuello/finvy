import 'package:flutter/material.dart';


class LoadingWidget extends StatelessWidget{
  double height;
  double width;
  double stroke;

  LoadingWidget(this.height, this.width, this.stroke);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Center(
        child: SizedBox(
          child: CircularProgressIndicator(strokeWidth: stroke),
          height: height,
          width: width,
        ),
      ),
    );
  }

}