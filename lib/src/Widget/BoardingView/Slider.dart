import 'package:flutter/cupertino.dart';

class Slider {
  final String sliderImageUrl;
  final String sliderHeading;
  final String sliderSubHeading;

  Slider(
      {@required this.sliderImageUrl,
        @required this.sliderHeading,
        @required this.sliderSubHeading});
}

final sliderArrayList = [
  Slider(
      sliderImageUrl: 'resources/images/uno.png',
      sliderHeading: "¿Necesitas un servicio urgente?",
      sliderSubHeading: "Contrary to popular belief, Lorem Ipsum is not simply random text."),
  Slider(
      sliderImageUrl: 'resources/images/tres.png',
      sliderHeading: "Encuentra a alguien cerca de ti",
      sliderSubHeading: "Contrary to popular belief, Lorem Ipsum is not simply random text."),
  Slider(
      sliderImageUrl: 'resources/images/dos.png',
      sliderHeading: "Aprovecha el talento de tu entorno",
      sliderSubHeading: "Contrary to popular belief, Lorem Ipsum is not simply random text."),
];