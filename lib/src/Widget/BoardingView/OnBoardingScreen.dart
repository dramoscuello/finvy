import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'SliderView.dart';


class LandingPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LandingPageState();
}


class _LandingPageState extends State<LandingPage> {

  void closeApp(){//si el usuario le da al boton de atras, la aplicacion se cierra y no envia a la vista anterior
    SystemNavigator.pop();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(//Este widget "WillPopScope" detecta cuando le dan al botón "atrás" y ejecuta una funcion que se le asigne
        onWillPop:(){
          closeApp();
        },
      child:Scaffold(
        backgroundColor: Colors.white,
        body: onBordingBody(), //el cuerpo de la vista es la funcion que se encuentra abajo, que retorna un conteneder y tiene un hijo,
        // que es un widget que esta en la clase SliderLayoutView()
      )
    );
  }

  Widget onBordingBody() => Container(
    child: SliderLayoutView(),
  );
}

