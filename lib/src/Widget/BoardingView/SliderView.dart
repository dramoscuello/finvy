import 'package:flutter/material.dart';
import 'package:whereappv2/src/utils/TextStyles.dart';
import '../../welcomePage.dart';
import 'sliderDots.dart';
import 'sliderItems.dart';
import 'Slider.dart';
import '../../config/shared_preferences_tour_viewed.dart';

class SliderLayoutView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SliderLayoutViewState();
}

class _SliderLayoutViewState extends State<SliderLayoutView> {
  //clase que se encarga de controlar el comportamiento del slider que tiene el tour, para pasar de una vista interna a otra
  int _currentPage = 0;
  final PageController _pageController = PageController(initialPage: 0);
  var tour = new TourViewed();

  void  NextView(){
    if(_currentPage < 2){
      setState(() {
        _currentPage++;
        _pageController.animateToPage(_currentPage, duration: Duration(milliseconds: 300), curve: Curves.linear);
        _pageController.jumpToPage(_currentPage);
      });
    }else{
      setState(() {
        _currentPage = 0;
        _pageController.animateToPage(_currentPage, duration: Duration(milliseconds: 300), curve: Curves.linear);
        _pageController.jumpToPage(_currentPage);
      });
    }

  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  _onPageChanged(int index) {
    setState(() {
      _currentPage = index;
    });
  }

  @override
  Widget build(BuildContext context) => topSliderLayout();

  Widget topSliderLayout() => Container(
    child: Padding(
        padding: EdgeInsets.all(10.0),
        child: Stack(
          alignment: AlignmentDirectional.bottomCenter,
          children: <Widget>[
            PageView.builder(
              scrollDirection: Axis.horizontal,
              controller: _pageController,
              onPageChanged: _onPageChanged,
              itemCount: sliderArrayList.length,
              itemBuilder: (ctx, i) => SlideItem(i),
            ),
            Stack(
              alignment: AlignmentDirectional.topStart,
              children: <Widget>[
                Align(
                  alignment: Alignment.bottomRight,
                  child: Padding(
                    padding: EdgeInsets.only(right: 15.0, bottom: 15.0),
                      child: FlatButton(child: BoldText("Siguiente", 15.5, Color(0xff000000)), onPressed: (){
                      NextView();
                    })
                  ),
                ),


                Align(
                  alignment: Alignment.bottomLeft,
                  child: Padding(
                    padding: EdgeInsets.only(left: 15.0, bottom: 15.0),
                    child: FlatButton(child: BoldText("Omitir", 15.5, Color(0xff000000)), onPressed: ()async{
                      //cuando le damos clic al boton "omitir" este ejecuta la funcion que se encuentra en la clase
                      //TourViewed() que se encarga de guardar localmente una variable booleana que nos dice que ya el tour se ha visto
                      await tour.Viewed();
                      await Navigator.push(context, MaterialPageRoute(builder: (context) => WelcomePage()));
                    })

                  ),
                ),


                Container(
                  alignment: AlignmentDirectional.bottomCenter,
                  margin: EdgeInsets.only(bottom: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      for (int i = 0; i < sliderArrayList.length; i++)
                        if (i == _currentPage)
                          SlideDots(true)
                        else
                          SlideDots(false)
                    ],
                  ),
                ),
              ],
            )
          ],
        )),
  );
}