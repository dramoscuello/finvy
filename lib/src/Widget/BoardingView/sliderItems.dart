import '../../config/shared_preferences_tour_viewed.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../utils/TextStyles.dart';
import '../../welcomePage.dart';
import 'Slider.dart';

class SlideItem extends StatelessWidget {
  final int index;
  var tour = new TourViewed();
  SlideItem(this.index);


  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.width * 0.6,
          width: MediaQuery.of(context).size.height * 0.8,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(sliderArrayList[index].sliderImageUrl))),
        ),
        SizedBox(
          height: 60.0,
        ),
        BoldText(sliderArrayList[index].sliderHeading, 20.5, Color(0xff000000)),
        SizedBox(
          height: 15.0,
        ),
        Center(
          child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 40.0),
              child: index != 2
                  ? NormalText(sliderArrayList[index].sliderSubHeading,Color(0xff000000), 14.5)
                  : Column(
                      children: <Widget>[
                        NormalText(sliderArrayList[index].sliderSubHeading,Color(0xff000000), 14.5),
                        SizedBox(
                          height: 50,
                        ),
                        InkWell(
                            onTap: () async{
                                await tour.Viewed();
                                await Navigator.push(context, MaterialPageRoute(builder: (context) => WelcomePage()));
                                //cuando se da tap al boton "unirme" se guarda localmente una variable booleana que nos dice que el tour ya se ha visto
                              //esta es la que se verifica en el middleware para que no la vuelva a mostrar, este metodo se encuentra en la clase TourViewed()
                              //archivo: "shared_preferences_tour_viewed.dart"
                            },
                            child:Container(
                              width: 200,
                              padding: EdgeInsets.symmetric(vertical: 15),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(45)),
                                  boxShadow: <BoxShadow>[
                                    BoxShadow(
                                        color: Colors.grey.shade200,
                                        offset: Offset(2, 4),
                                        blurRadius: 5,
                                        spreadRadius: 2)
                                  ],
                                  gradient: LinearGradient(
                                      begin: Alignment.centerLeft,
                                      end: Alignment.centerRight,
                                      colors: [Color(0xff977EF2), Color(0xff2955D9)])),
                              child: BoldText("¡Unirme!", 20, Color(0xffffffff)),
                            )
                        )
                      ],
                    )),
        ),
      ],
    );
  }
}
