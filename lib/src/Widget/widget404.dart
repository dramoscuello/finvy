import 'package:flutter/material.dart';


class Widget404 extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Center(
        child: Image(
          image: AssetImage('resources/images/not_found.png'),
          height: 300,
          width: 280,
        ),
      ),
    );
  }

}