import 'package:whereappv2/src/Widget/ProgresDialog.dart';
import 'package:whereappv2/src/config/sqlite_info_user.dart';
import 'package:whereappv2/src/screens/homeScreen.dart';
import 'package:whereappv2/src/utils/TextStyles.dart';


import 'signup.dart';
import 'package:flutter/material.dart';
import 'Widget/backButton.dart';
import 'package:encrypt/encrypt.dart' as enk;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class LoginPage extends StatefulWidget {
  bool SuccesRegister;
  LoginPage([this.SuccesRegister]);
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {


  TextEditingController txtUsuario = TextEditingController();//controladores para obtener los valores de los campos de texto
  TextEditingController txtPass = TextEditingController();
  bool HidePass = true;
  //PARAMETROS PARA ENCRIPTAR
  final key = enk.Key.fromUtf8('zxcvbnmasdfghjkl1234567890qwerty');//llave de encriptacion de contraseñas
  final iv = enk.IV.fromLength(16);



  GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();

  var managedb = new ManageDb();
  var showD = new showProgressDialog();


  Login(String username, String password){
    showD.showDialog(context, "Espere...");
    FirebaseFirestore.instance
        .collection('users_')
        .where('username', isEqualTo:username)
        .where('pass', isEqualTo:password)
        .where('rol', whereIn:[1,3])
        .get()
        .then((QuerySnapshot querySnapshot) => {
        if(querySnapshot.docs.isEmpty){
          showD.hideShowDialog(),
          snackMsj("Usuario o contraseña incorrectos", Colors.red)
        }else{
          saveDataLocal(querySnapshot.docs.first)
        }
    });
  }

  saveDataLocal(data)async{
    await managedb.createDb();
    await managedb.createTableUser();
    await managedb.insertInTableUser(int.parse(data.id), '${data['name']}', '${data['last_name']}', '${data['date_born']}', '${data['phone']}',
        '${data['email']}', '${data['username']}', '${data['pass']}', '${data['estado']}', '${data['picture']}', '${data['reputation']}' , '${data['njobdone']}',
        '${data['lang']}', '${data['available']}', '${data['rol']}', '${data['accept_terms_us']}', '${data['accept_terms_serv']}');

    await showD.hideShowDialog();
    await Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen()));

  }


  void snackMsj(String msj, Color color){//funcion que muestra mensajes snack, aquello que se muestran en la parte inferior del dispositivo
    _scaffold.currentState.showSnackBar(SnackBar(content: Text(msj),
        backgroundColor:color));
  }


  Future<void> initializeDefault() async {
    FirebaseApp app = await Firebase.initializeApp();
    assert(app != null);
    print('Initialized default app $app');
  }



  @override
  void initState() {
    initializeDefault();
  }

  Widget entryField1() {//campo de texto para el usuario
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText("Usuario",Color(0xff000000), 18),
          SizedBox(
            height: 10,
          ),
          TextField(
             textInputAction: TextInputAction.next,
              controller: txtUsuario,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  fillColor: Color(0xfff3f3f4),
                  filled: true,
                  prefixIcon: Icon(FontAwesomeIcons.user)
              ),)
        ],
      ),
    );
  }

  Widget entryField2() {//campo de texto para contraseña
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText("Password",Color(0xff000000), 18),
          SizedBox(
            height: 10,
          ),
          TextField(
            obscureText: HidePass,
            controller: txtPass,
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true,
                prefixIcon: Icon(FontAwesomeIcons.key),
                suffixIcon: IconButton(onPressed: (){//cuando se le de tap al icono del ojo, se mostrará la contraseña
                  //para ello usamos una variable, HidePass, el cual comienza con true y se utiliza la variable para decirle a este campo de texto que si será un campo
                  //contraseña, de este modo, cuando le cambiemos el estado a la variable, cambiará tambien para la caracteristica "obscureText" que es la que oculta
                  //el contenido del campo de texto como lo hace un campo de texto contraseña
                  setState(() {
                    HidePass = !HidePass;
                  });
                }
                ,icon: HidePass?Icon(FontAwesomeIcons.eye):Icon(FontAwesomeIcons.eyeSlash))
            ),
            onSubmitted: (text){
              submitData();
            },
          )
        ],
      ),
    );
  }

  void submitData(){
    if(txtUsuario.text.trim() != "" && txtPass.text.trim() != ""){
      final encrypter = enk.Encrypter(enk.AES(key));
      final encrypted_pass = encrypter.encrypt(txtPass.text.trim(), iv: iv);
      Login(txtUsuario.text.trim(), encrypted_pass.base64);
    }else{
      snackMsj("TODOS LOS CAMPOS SON REQUERIDOS", Colors.red);
    }
  }


  Widget _submitButton() {
    return InkWell(
        onTap: () {
          //cuando se le da clic al boton login, verifica que no esten vacios y luego concripta la contraseña y envia los datos la funcion login
          submitData();
        },
        child:Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(vertical: 15),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(45)),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.grey.shade200,
                    offset: Offset(2, 4),
                    blurRadius: 5,
                    spreadRadius: 2)
              ],
              gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [Color(0xff977EF2), Color(0xff2955D9)])),
          child: NormalText("Iniciar sesión",Color(0xffffffff), 20.5),
        )
    );
  }



  Widget _createAccountLabel() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20),
      alignment: Alignment.bottomCenter,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          NormalText("¿No tienes cuenta?",Color(0xff000000), 15),
          SizedBox(
            width: 10,
          ),
          InkWell(
            onTap: () {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => SignUpPage()));
            },
            child: NormalText("¡Registrate!",Color(0xff1A44F1), 15),
          )
        ],
      ),
    );
  }

  Widget _title() {
    return BoldText("Fisvy", 40, Color(0xfF000000));
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      key: _scaffold,
        body: SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height,
              child: Stack(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          flex: 3,
                          child: SizedBox(),
                        ),
                        _title(),
                        SizedBox(
                          height: 50,
                        ),
                        entryField1(),
                        entryField2(),
                        SizedBox(
                          height: 20,
                        ),
                        _submitButton(),

                        Container(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          alignment: Alignment.centerRight,
                          child: InkWell(
                            onTap: () {
                              //Navigator.pushReplacement(context,
                                //  MaterialPageRoute(builder: (context) => SignUpPage()));
                            },
                            child: NormalText("¿Olvidaste tu contraseña?",Color(0xff1A44F1), 15),
                          ),

                        ),
                        //_divider(),
                        Expanded(
                          flex: 2,
                          child: SizedBox(),
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: _createAccountLabel(),
                  ),
                  Positioned(top: 40, left: 0, child: backButton())
                ],
              ),
            )
        )
    );
  }
}
