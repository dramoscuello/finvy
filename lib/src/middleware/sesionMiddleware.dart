import 'package:firebase_core/firebase_core.dart';

import '../Widget/BoardingView/OnBoardingScreen.dart';
import '../screens/homeScreen.dart';
import 'package:flutter/material.dart';
import '.././welcomePage.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as pj; //se le asignó un alias al paquete ya que existen paquetes internos que usan la palabra "path"
import 'package:shared_preferences/shared_preferences.dart';
/*Cuando en los imports hayan errores, revisar el archivo "pubspec.yaml", en el cual se instalan librerias y assetss*/




class sesionMiddleware extends StatefulWidget{
  /*las clases que extienden a StatefulWidget, quiere decir que la aplicacion es susceptible a cambios en la vista,
  en cambio, las clases que extienden a StatelessWidget, son vistas estáticas
  * */
  @override
  _StateSesionMiddleware createState() => _StateSesionMiddleware();

}

class _StateSesionMiddleware extends State<sesionMiddleware>{
  bool existDbU;
  bool viewedTour;


  @override
  Widget build(BuildContext context) { //el medoto sobre cargado build, nos retorna la vista de la app, en este caso, nos muestra solo el
    //logo de la aplicación
    return Scaffold(
      body: Container(
        child: Center(
            child: Image(
              image: AssetImage('resources/images/logo.png'),
              height: 80,
              width: 80,
            )
        ),
      ),
    );
  }

  Future<bool> check_shared_preferences()async{//funcion asincrona que verifica si existe un valor guardado (este nos diria si ya se ha visto el tour de la app"
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {//se actualiza el estado o valor a la variable dependiendo de la respuesta del metodo que verifica si ya se ha visto el tour
      viewedTour = prefs.getBool('viewed') ?? false;
    });
  }

  Future<bool> check_local_database()async{//funcion asincrona que verifica si existe una base de datos local, donde se encontraria la informacion de usuario logeado
    bool response;
    var databasesPath;
    String pathU;

    databasesPath = await getDatabasesPath();
    pathU = pj.join(databasesPath, 'db_user_info.db');
    response = await databaseExists(pathU);
    setState(() {//se cambia el estado de la variable y se le asigna el valor que retorna el metodo que verifica si existe base de datos
      existDbU = response;
    });
  }


  Future checkSesion()async{//función asincrona, que al iniciar ejecuta las dos funciones siguientes
    await check_shared_preferences();
    await check_local_database();

    if(viewedTour){
      if(existDbU){
        await Firebase.initializeApp();
        Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen()));
        //Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeScreen()));
      }else{//sino existe base de datos local, se reemplaza la vista actual por la bienvenida donde podrá iniciar sesión o registrarse
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => WelcomePage()));
      }
    }else{//si el usuario no ha visto el tour, se reemplaza la vista actual, por aquella vista
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LandingPage()));
    }
  }



  @override
  void initState() {//metodo sobrecargado que se ejecuta cuandio inicia la vista actual
    super.initState();
    Future.delayed(Duration(seconds:2),(){//después de dos segundos ejecuta la función "checkSesion()"
      checkSesion();
    });
  }
}