import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class ManageDb{
  var db;
  var databasesPath;
  String path;
  Database database;


  Future createDb()async{
    databasesPath = await getDatabasesPath();
    path = join(databasesPath, 'db_user_info.db');
  }

  Future DeleteDb()async{
    databasesPath = await getDatabasesPath();
    path = join(databasesPath, 'db_user_info.db');
    await deleteDatabase(path);
  }

  Future createTableUser()async{
    database = await openDatabase(path, version: 1, onCreate: (Database db, int version) async {
          await db.execute(
              'CREATE TABLE UserInfo (id_user INTEGER PRIMARY KEY, name TEXT, last_name TEXT,'
                  ' date_born TEXT, phone TEXT, email TEXT, username TEXT, password TEXT, estado TEXT,'
                  ' picture TEXT, reputacion TEXT,'
                  'njobdone TEXT, lang TEXT, available TEXT, rol TEXT, terms TEXT, terms_serv TEXT)');
        });
  }

  Future insertInTableUser(int idu, String name, String last, String date, String phone, String email,
      String user, String password, String estado, String pic, String rep, String nj, String lang, String av, String rol, String terms, String terms_serv)async{
      await database.transaction((txn) async {
        int id = await txn.rawInsert(
            'INSERT INTO UserInfo (id_user, name, last_name, date_born, phone, email, username, password,  estado, picture,'
                'reputacion, njobdone, lang, available, rol, terms, terms_serv) VALUES(?, ?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?,?,?)',
            [
              idu,
              name,
              last,
              date,
              phone,
              email,
              user,
              password,
              estado,
              pic,
              rep,
              nj,
              lang,
              av,
              rol,
              terms,
              terms_serv
            ]);
        print('Insertado: $id');
      });
  }

  Future AddNewService(int idp, int nserv)async{
    await createDb();
    database = await openDatabase(path);
      int count = await database.rawUpdate(
          'UPDATE UserInfo SET nservices = ? WHERE id_profile = ?',
          [nserv+1, idp]);
      print('updated: $count');
  }

  Future UpdateAvailable(int idp, String av)async{
    await createDb();
    database = await openDatabase(path);
    int count = await database.rawUpdate(
        'UPDATE UserInfo SET available = ? WHERE id_user = ?',
        ['${av}', idp]);
    print('updated: $count');
  }

  Future UpdateDateBorn(int id_user, String fecha)async{
    await createDb();
    database = await openDatabase(path);
    int count = await database.rawUpdate(
        'UPDATE UserInfo SET date_born = ? WHERE id_user = ?',
        ['${fecha}', id_user]);
    print('updated: $count');
  }

  Future UpdatePicture(int id_user, String img)async{
    await createDb();
    database = await openDatabase(path);
    int count = await database.rawUpdate(
        'UPDATE UserInfo SET picture = ? WHERE id_user = ?',
        ['${img}', id_user]);
    print('updated: $count');
  }

  Future UpdateNameYLast(int id_user, String name, String last)async{
    await createDb();
    database = await openDatabase(path);
    int count = await database.rawUpdate(
        'UPDATE UserInfo SET name = ?, last_name = ? WHERE id_user = ?',
        ['${name}', '${last}', id_user]);
    print('updated: $count');
  }

  Future UpdateUsername(int id_user, String username)async{
    await createDb();
    database = await openDatabase(path);
    int count = await database.rawUpdate(
        'UPDATE UserInfo SET username = ? WHERE id_user = ?',
        ['${username}', id_user]);
    print('updated: $count');
  }

  Future UpdateEmail(int id_user, String email)async{
    await createDb();
    database = await openDatabase(path);
    int count = await database.rawUpdate(
        'UPDATE UserInfo SET email = ? WHERE id_user = ?',
        ['${email}', id_user]);
    print('updated: $count');
  }

  Future UpdateTel(int id_user, String tel)async{
    await createDb();
    database = await openDatabase(path);
    int count = await database.rawUpdate(
        'UPDATE UserInfo SET phone = ? WHERE id_user = ?',
        ['${tel}', id_user]);
    print('updated: $count');
  }

  Future UpdatePass(int id_user, String pass)async{
    await createDb();
    database = await openDatabase(path);
    int count = await database.rawUpdate(
        'UPDATE UserInfo SET password = ? WHERE id_user = ?',
        ['${pass}', id_user]);
    print('updated: $count');
  }

  Future UpdateTermsServ(int idp, String av)async{
    await createDb();
    database = await openDatabase(path);
    int count = await database.rawUpdate(
        'UPDATE UserInfo SET terms_serv = ? WHERE id_user = ?',
        ['${av}', idp]);
    print('updated: $count $av');
  }

  Future<List<Map>> ListUserInfo()async{
      await createDb();
      database = await openDatabase(path);
      List<Map> list = await database.rawQuery('SELECT * FROM UserInfo');
      return list;
  }

  Future<List<Map>> get_pass()async{
    await createDb();
    database = await openDatabase(path);
    List<Map> list = await database.rawQuery('SELECT password FROM UserInfo');
    return list;
  }
}