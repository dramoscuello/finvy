import 'package:shared_preferences/shared_preferences.dart';

class TourViewed{
  Future Viewed()async{ //funcion asincrona que guarda de maneral local y persistente el valor true, a la variable "viewed"
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('viewed', true);
  }
}