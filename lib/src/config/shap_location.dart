import 'package:shared_preferences/shared_preferences.dart';

class LocationSaved{
  Future LocationSave(String loc)async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('location', loc);
  }
}