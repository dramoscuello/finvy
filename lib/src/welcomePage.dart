import 'package:whereappv2/src/utils/TextStyles.dart';

import 'signup.dart';
import 'package:flutter/material.dart';
import 'loginPage.dart';
import 'package:flutter/services.dart';

class WelcomePage extends StatefulWidget {
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  //clase que nos retorna la vista de bienvenida, en el cual se encuentran dos botones "iniciar sesion" y "registrarse"

  Widget _submitButton() {
    return InkWell(
      onTap: () {
        //cuando se le da tap a este boton, nos muestra la vista de login
        //tener presente que para redireccionar a otra vista se puede usar push, como en este caso o pushReplacement
        //push: superpone la siguiente vista encima de la actual, asi que cuando le demos al boton atras, la de arriba se cerrará y nos mostrará la vista anterior
        //pushReplacement: reemplaza la vista actual por la siguiente
       Navigator.push(context, MaterialPageRoute(builder: (context) => LoginPage(false)));
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 13),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(50)),
            color: Color(0xffffffff)),
        child: NormalText("Iniciar sesión",Color(0xff000000), 20.5),
      ),
    );
  }

  Widget _signUpButton() {
    return InkWell(
      onTap: () {
        //cuando se selecciona este boton, nos muestra la vista de registro
        Navigator.push(context, MaterialPageRoute(builder: (context) => SignUpPage()));
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 13),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(50)),
          border: Border.all(color: Color(0xffffffff), width: 2),
        ),
        child: NormalText("¡Registrate!",Color(0xffffffff), 20.5),
      ),
    );
  }

  Widget _title() {
    return BoldText("Fisvy", 40, Color(0xffffffff));
  }

  void closeApp(){
    SystemNavigator.pop();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop:(){
        closeApp();
      },
      child: Scaffold(
        body:SingleChildScrollView(
          child:Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.grey.shade200,
                      offset: Offset(2, 4),
                      blurRadius: 5,
                      spreadRadius: 2)
                ],
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [Color(0xff977EF2), Color(0xff2955D9)])),
            child: Column(// en esta columna de instancian los botones que se hicieron afuera "login" y "register"
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _title(),
                SizedBox(
                  height: 80,
                ),
                _submitButton(),
                SizedBox(
                  height: 20,
                ),
                _signUpButton(),
                SizedBox(
                  height: 20,
                ),
                //_label()
              ],
            ),
          ),
        ),
      )
    );
  }


}
