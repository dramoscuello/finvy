import 'dart:convert';
import 'package:whereappv2/src/loginPage.dart';
import 'package:whereappv2/src/utils/TextStyles.dart';
import 'package:whereappv2/src/utils/profile_user_default.dart';

import 'package:flutter/material.dart';
import 'Widget/ProgresDialog.dart';
import 'Widget/backButton.dart';
import 'Widget/signup/loginAccountLabel.dart';
import 'package:email_validator/email_validator.dart';
import 'package:encrypt/encrypt.dart' as enk;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';



class SignUpPage extends StatefulWidget {
  SignUpPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _SignUpPageState createState() => _SignUpPageState();
}



class _SignUpPageState extends State<SignUpPage> {

  bool HidePass1 = true;
  bool HidePass2 = true;

  var showD = new showProgressDialog();


  //PARAMETROS PARA ENCRIPTAR
  final key = enk.Key.fromUtf8('zxcvbnmasdfghjkl1234567890qwerty');
  final iv = enk.IV.fromLength(16);

  //MENSAJES DE ERROR EN EL REGISTRO
  String errorId = '';
  String errorUsername = '';
  String errorPass = '';
  String errorPass2 = '';
  String errorEmail = '';
  String errorCode = '';
  String errorName = '';
  String errorLast = '';

  //ENCRIPTED PASSWORDS
  String pass1;


  //SCAFFOLD KEY - ESTA ES LA LLAVE UNICA PARA EL SCAFFOLD
  final GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();

  //CONTROLADORES TEXTeDIT
  TextEditingController txtId = TextEditingController();
  TextEditingController txtName = TextEditingController();
  TextEditingController txtLast = TextEditingController();
  TextEditingController txtEmail = TextEditingController();
  TextEditingController txtUsuario = TextEditingController();
  TextEditingController txtPass = TextEditingController();
  TextEditingController txtPass2 = TextEditingController();
  TextEditingController txtCode = TextEditingController();


  Utf8Codec utf8 = Utf8Codec();
  bool accept = false;
  int pos = 0;

  bool verifingUser = false;
  bool verifingEmail = false;
  bool isTypingUser = false;
  bool isTypingEmail = false;
  bool isTypingId = false;
  bool verifingId = false;
  //FIN DE LOS ATRIBUTOS



  Future saveUser()async{
    CollectionReference users = FirebaseFirestore.instance.collection('users_');
    return users
        .doc(txtId.text.trim())
        .set({
      'name': txtName.text.trim(),
      'last_name': txtLast.text.trim(),
      'date_born':'',
      'phone':'',
      'email':txtEmail.text.trim(),
      'username': txtUsuario.text.trim(),
      'pass': pass1,
      'estado':true,
      'picture': pic_default,
      'reputation':5,
      'njobdone':0,
      'lang':'',
      'available':false,
      'location':GeoPoint(9.2320559, -75.8166036),
      'idphoto1':'',
      'idphoto2':'',
      'facephoto':'',
      'rol':1,
      'about':'',
      'accept_terms_us':true,
      'accept_terms_serv':false,
      'createdAt':Timestamp.fromDate(DateTime.now()),
      'updatedAt':''
    })
    .then((value) => {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LoginPage(true)))
    })
    .catchError((error) => print("Failed to add user: $error"));
  }

  Future<void> initializeDefault() async {
    FirebaseApp app = await Firebase.initializeApp();
    assert(app != null);
    print('Initialized default app $app');
  }



  @override
  void initState() {
    initializeDefault();
  }

  void snackMsj(String msj, Color color){
    _scaffold.currentState.showSnackBar(SnackBar(content: Text(msj),
        backgroundColor:color));
  }



  void checkEqualsPass(){
    if(txtPass2.text !=  txtPass.text){
      setState(() {
        errorPass2 = 'Las contraseñas no coinciden';
      });
    }else{
      setState(() {
        errorPass2 = '';
      });
    }
  }

  checkId(){
    if(txtId.text.length < 8){
      setState(() {
        errorId = 'Debe contener minimo 8 digitos';
        isTypingId = false;
        verifingId = false;
      });
    }else {
      setState(() {
        isTypingId = true;
        verifingId = true;
      });
      FirebaseFirestore.instance
          .collection('users_')
          .doc(txtId.text.trim())
          .get()
          .then((DocumentSnapshot documentSnapshot) {
        if (documentSnapshot.exists) {
          setState(() {
            verifingId = true;
            errorId = 'Ya existe un usuario con esta identificación';
          });
        } else {
          setState(() {
            verifingId = false;
            errorId = '';
          });
        }
      });
    }
  }

  checkEmail(){
    bool isValid = EmailValidator.validate(txtEmail.text);
    if (isValid){
      setState(() {
        isTypingEmail = true;
        verifingEmail = true;
      });
      FirebaseFirestore.instance
          .collection('users_')
          .where('email', isEqualTo:txtEmail.text.trim())
          .get()
          .then((QuerySnapshot querySnapshot) => {
        if(querySnapshot.docs.isEmpty){
          setState(() {
            errorEmail = '';
            verifingEmail = false;
          })
        }else{
          setState(() {
            errorEmail = 'Ya existe un registro con este Email';
            verifingEmail = true;
          })
        }
      })
          .catchError((error) => print("Failed: $error"));
    }else{
      setState(() {
        errorEmail = 'Email no valido';
        isTypingEmail = false;
        verifingEmail = false;
      });
    }
  }

  checkUsername(){
    if(txtUsuario.text.length < 4){
      setState(() {
        errorUsername = 'Debe contener minimo 4 caracteres';
        isTypingUser = false;
        verifingUser = false;
      });
    }else{
      setState(() {
        isTypingUser = true;
        verifingUser = true;
      });

      FirebaseFirestore.instance
          .collection('users_')
          .where('username', isEqualTo:txtUsuario.text.trim())
          .get()
          .then((QuerySnapshot querySnapshot) => {
        if(querySnapshot.docs.isEmpty){
          setState(() {
            errorUsername = '';
            verifingUser = false;
          })
        }else{
          setState(() {
            errorUsername = 'Ya existe un registro con este username';
            verifingUser = true;
          })
        }
      })
          .catchError((error) => print("Failed: $error"));
    }
  }


  Widget entryField1() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText("Identificación",Color(0xff000000), 18),
          SizedBox(
            height: 10,
          ),
          TextField(
            keyboardType: TextInputType.number,
            controller: txtId,
            textInputAction: TextInputAction.next,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true,
                prefixIcon: Icon(FontAwesomeIcons.passport),
                suffixIcon: IconButton(onPressed: (){
                  print("OK");
                }
                    ,icon: isTypingId?verifingId?Icon(FontAwesomeIcons.clock):Icon(FontAwesomeIcons.check):Container()),
                errorText: errorId,
                errorStyle: TextStyle(
                    color: Colors.red,
                    fontSize: 14,
                    fontWeight: FontWeight.bold
                )
            ),
            autocorrect:false,
            onChanged: (text){
              checkId();
            },
          )
        ],
      ),
    );
  }
  Widget entryField2() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText("Nombres",Color(0xff000000), 18),
          SizedBox(
            height: 10,
          ),
          TextField(
            keyboardType: TextInputType.text,
            controller: txtName,
            textInputAction: TextInputAction.next,
            decoration: InputDecoration(
              border: InputBorder.none,
              fillColor: Color(0xfff3f3f4),
              filled: true,
              prefixIcon: Icon(FontAwesomeIcons.signature),
              errorText: errorName,
            ),
            autocorrect:false,
          )
        ],
      ),
    );
  }
  Widget entryField3() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText("Apellidos",Color(0xff000000), 18),
          SizedBox(
            height: 10,
          ),
          TextField(
            keyboardType: TextInputType.text,
            controller: txtLast,
            textInputAction: TextInputAction.next,
            decoration: InputDecoration(
              border: InputBorder.none,
              fillColor: Color(0xfff3f3f4),
              filled: true,
              prefixIcon: Icon(FontAwesomeIcons.signature),
              errorText: errorLast,
            ),
            autocorrect:false
          ),

        ],
      ),
    );
  }
  Widget entryField4() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText("E-mail",Color(0xff000000), 18),
          SizedBox(
            height: 10,
          ),
          TextField(
            keyboardType: TextInputType.emailAddress,
            textInputAction: TextInputAction.next,
            controller: txtEmail,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true,
                prefixIcon: Icon(FontAwesomeIcons.envelopeOpenText),
                suffixIcon: IconButton(onPressed: (){
                  print("OK");
                }
                    ,icon: isTypingEmail?verifingEmail?Icon(FontAwesomeIcons.clock):Icon(FontAwesomeIcons.check):Container()),
                errorText: errorEmail,
                errorStyle: TextStyle(
                    color: Colors.red,
                    fontSize: 14,
                    fontWeight: FontWeight.bold
                )
            ),
            autocorrect:false,
            onChanged: (text){
              checkEmail();
            },
          )
        ],
      ),
    );
  }
  Widget entryField5() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText("Username",Color(0xff000000), 18),
          SizedBox(
            height: 10,
          ),
          TextField(
            keyboardType: TextInputType.text,
            controller: txtUsuario,
            textInputAction: TextInputAction.next,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true,
                prefixIcon: Icon(FontAwesomeIcons.users),
                suffixIcon: IconButton(onPressed: (){
                  print("OK");
                }
                    ,icon: isTypingUser?verifingUser?Icon(FontAwesomeIcons.clock):Icon(FontAwesomeIcons.check):Container()),
                errorText: errorUsername,
                errorStyle: TextStyle(
                    color: Colors.red,
                    fontSize: 14,
                    fontWeight: FontWeight.bold
                )
            ),
            autocorrect:false,
            onChanged: (text){
             checkUsername();
            },

          )
        ],
      ),
    );
  }
  Widget entryField6() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText("Password",Color(0xff000000), 18),
          SizedBox(
            height: 10,
          ),
          TextField(
            obscureText: HidePass1,
            keyboardType: TextInputType.text,
            controller: txtPass,
            textInputAction: TextInputAction.next,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true,
                prefixIcon: Icon(Icons.vpn_key),
                suffixIcon: IconButton(
                  icon: HidePass1?Icon(FontAwesomeIcons.eye):Icon(FontAwesomeIcons.eyeSlash),
                  onPressed: (){
                    setState(() {
                      HidePass1 = !HidePass1;
                    });
                  },
                ),
                errorText: errorPass,
                errorStyle: TextStyle(
                    color: Colors.red,
                    fontSize: 14,
                    fontWeight: FontWeight.bold
                )
            ),
            autocorrect:false,
            onChanged: (text){
              if(txtPass.text.length < 8){
                setState(() {
                  errorPass = 'Debe contener minimo 8 caracteres';
                });
              }else {
                setState(() {
                  errorPass = '';
                });
                if(txtPass2.text != ""){
                  checkEqualsPass();
                }
              }
            },
          )
        ],
      ),
    );
  }
  Widget entryField7() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText("Repetir password",Color(0xff000000), 18),
          SizedBox(
            height: 10,
          ),
          TextField(
            obscureText: HidePass2,
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.next,
            controller: txtPass2,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true,
                prefixIcon: Icon(Icons.vpn_key),
                suffixIcon: IconButton(
                  icon: HidePass2?Icon(FontAwesomeIcons.eye):Icon(FontAwesomeIcons.eyeSlash),
                  onPressed: (){
                    setState(() {
                      HidePass2 = !HidePass2;
                    });
                  },
                ),
                errorText: errorPass2,
                errorStyle: TextStyle(
                    color: Colors.red,
                    fontSize: 14,
                    fontWeight: FontWeight.bold
                )
            ),
            autocorrect:false,
            onChanged: (text){
             checkEqualsPass();
            },
          )
        ],
      ),
    );
  }
  Widget entryField8() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 55, vertical: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Checkbox(
              value: accept,
              onChanged: (value){
                setState(() {
                  accept = !accept;
                });
              }),
          Expanded(
              child: InkWell(
                onTap: () {
                },
                child: NormalText("Aceptar términos y condiciones",Color(0xff1A44F1), 16),
              )
          ),
        ],
      ),
    );
  }


  Widget _title() {
    return BoldText("Fisvy", 40, Color(0xfF000000));
  }


  Widget _backButton() {
    return InkWell(
        onTap: () {
          if(pos>0){
            setState(() {
              pos--;
            });
          }
        },
        child:Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(vertical: 5),
          margin: EdgeInsets.symmetric(horizontal: 5),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(45)),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.grey.shade200,
                    offset: Offset(2, 4),
                    blurRadius: 5,
                    spreadRadius: 2)
              ],
              gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [Color(0xff977EF2), Color(0xff2955D9)])),
          child: Icon(Icons.arrow_back, color: Colors.white, size: 40),
        )
    );
  }

  Widget _nextButton() {
    return InkWell(
        onTap: () {
          switch(pos){
            case 0:{
              if(txtId.text != "" && txtName.text != "" && txtLast.text != ""){
                if(errorId == '' && !verifingId){
                  setState(() {
                    pos++;
                  });
                }
              }else{
                snackMsj("TODOS LOS CAMPOS SON REQUERIDOS",Colors.red);
              }
            }
            break;
            case 1: {
              if(txtEmail.text != "" && txtUsuario.text != ""){
                if(errorUsername == '' && errorEmail == '' && !verifingEmail && !verifingUser){
                  setState(() {
                    pos++;
                  });
                }
              }else{
                snackMsj("TODOS LOS CAMPOS SON REQUERIDOS",Colors.red);
              }
            }
            break;
            case 2:{
              if(txtPass.text != "" && txtPass2.text != ""){
                if(errorPass == '' && errorPass2 == ''){
                  if(accept){
                    final encrypter = enk.Encrypter(enk.AES(key));
                    final encrypted_pass1 = encrypter.encrypt(txtPass.text, iv: iv);
                    setState(() {
                      pass1 =  encrypted_pass1.base64;
                    });
                    saveUser();
                  }else{
                    snackMsj("DEBE ACEPTAR LOS TERMINOS Y RESTRICCIONES",Colors.red);
                  }
                }
              }else{
                snackMsj("TODOS LOS CAMPOS SON REQUERIDOS",Colors.red);
              }
            }
            break;
          }
        },
        child:Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(vertical: 5),
          margin: EdgeInsets.symmetric(horizontal: 5),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(45)),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.grey.shade200,
                    offset: Offset(2, 4),
                    blurRadius: 5,
                    spreadRadius: 2)
              ],
              gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [Color(0xff977EF2), Color(0xff2955D9)])),
          child: pos<2?Icon(Icons.arrow_forward, color: Colors.white, size: 40):Icon(Icons.check, color: Colors.white, size: 40),
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        backgroundColor: Colors.white,
        key: _scaffold,
        body: SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height,
              child: Stack(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          flex: 3,
                          child: SizedBox(),
                        ),
                        _title(),
                        SizedBox(
                          height: 50,
                        ),
                        Visibility(
                            visible: pos==0?true:false,
                            child: Wrap(
                              children: [
                                entryField1(),
                                entryField2(),
                                entryField3()
                              ],
                            )
                        ),
                        Visibility(
                            visible: pos==1?true:false,
                            child: Wrap(
                              children: [
                                entryField4(),
                                entryField5(),
                              ],
                            )
                        ),
                        Visibility(
                            visible: pos==2?true:false,
                            child: Wrap(
                              children: [
                                entryField6(),
                                entryField7(),
                                entryField8()
                              ],
                            )
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Expanded(
                                  flex: 1,
                                  child: Visibility(
                                    visible: pos>0?true:false,
                                    child: _backButton(),
                                  )
                              ),
                              Expanded(
                                flex: 1,
                                child: _nextButton(),
                              )
                            ]
                        ),
                        Expanded(
                          flex: 2,
                          child: SizedBox(),
                        ),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: loginAccountLabel(),
                        ),
                      ],
                    ),
                  ),
                  Positioned(top: 40, left: 0, child: backButton())
                ],
              ),
            )
        )
    );
  }
}


