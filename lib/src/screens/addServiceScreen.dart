import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:whereappv2/src/utils/TextStyles.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';



class addService extends StatefulWidget{
  List servs;
  var id_user;

  addService(this.servs, this.id_user);

  @override
  addServiceState createState() => addServiceState();
}

class addServiceState extends State<addService>{
  var arrayItems = [];
  var arraySearch = [];

  TextEditingController txtSearch = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();
  bool isSearching = false;
  CollectionReference user = FirebaseFirestore.instance.collection('_users_services');



  void BackAction(){
    if(isSearching){
      setState(() {
        isSearching = false;
      });
    }else{
      Navigator.pop(context);
    }
  }

  @override
  void initState() {
    arraySearch = widget.servs;
    arrayItems = widget.servs;
    initializeDefault();
  }

  Future<void> initializeDefault() async {
    FirebaseApp app = await Firebase.initializeApp();
    assert(app != null);
    print('Initialized default app $app');
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(
        onWillPop:(){
          BackAction();
        },
        child:Scaffold(
            key: _scaffold,
            backgroundColor: Colors.white,
            appBar: AppBar(
              backgroundColor:isSearching?Color(0xffffffff):Color(0xff977EF2),
              leading: IconButton(icon:Icon(Icons.arrow_back, color: isSearching?Colors.black:Colors.white,),
                  onPressed: (){Navigator.pop(context);}),
              title: isSearching?
              TextField(
                controller: txtSearch,
                autofocus: true,
                style:TextStyle(fontFamily: 'Amaranth', fontSize: 20, color: Colors.black),
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: "Buscar...",
                  hintStyle: TextStyle(fontFamily: 'Amaranth', fontSize: 20, color: Colors.black),
                ),
                onChanged: (text){
                  ChangedTextSearch(text);
                },
              ):
              NormalText("Buscar servicios...",Color(0xffffffff), 20),
              actions: <Widget>[
                IconButton(icon: isSearching?Icon(FontAwesomeIcons.times, color: Colors.black):Icon(FontAwesomeIcons.search),onPressed: (){
                  if(isSearching){
                    setState(() {
                      arrayItems = arraySearch;
                      txtSearch.text = '';
                    });
                  }

                  setState(() {
                    isSearching = !isSearching;
                  });
                }
                )
              ],
            ),
            body: Container(
                child: ListView.separated(
                    padding: EdgeInsets.fromLTRB(10.0,10.0,10.0,0),
                    itemCount: arrayItems.length,
                    itemBuilder:(context, index){
                      return Container(
                          margin: EdgeInsets.fromLTRB(0, 2, 0, 2),
                          padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                blurRadius: 10.0,
                                color: Colors.grey[350],
                              )
                            ],
                          ),
                          child:ListTile(
                            title: NormalText(arrayItems[index]['profession'],Color(0xff000000), 18),
                            leading: Image.memory(Base64Decoder().convert(arrayItems[index]['icon'].split(',').last), width: 40, height: 40),
                            trailing: Icon(Icons.add_box),
                            onTap: ()async{
                              setState(() {
                                isSearching = false;
                              });
                              _showMyDialog(arrayItems[index].id);
                            },
                          )
                      );
                    },
                    separatorBuilder: (context, index){
                      return Divider();
                    }
                )
            )
        )
    );
  }

  void ChangedTextSearch(String texto){
    if(texto.length > 2) {
      setState(() {
        arrayItems = [];
      });
      for (var i = 0; i < arraySearch.length; i++) {
        if (arraySearch[i]['tags'].toString().contains(texto)) {
          setState(() {
            arrayItems.add(arraySearch[i]);
          });
        }
      }
    }else{
      setState(() {
        arrayItems = arraySearch;
      });
    }
  }

  Future<void> _showMyDialog(id_profession) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25.0))
          ),
          title: NormalText("¿Confirmar?",Color(0xff000000), 22),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                NormalText('¡Se añadirá este servicio a su curriculum!',Color(0xff000000), 18),

              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child:  NormalText('Cancelar',Colors.lightBlue, 18),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: NormalText('OK',Colors.lightBlue, 18),
              onPressed: () async{
                Navigator.of(context).pop();
                add_service(id_profession);
              },
            )
          ],
        );
      },
    );
  }

  add_service(id_profession){

    return user
        .doc(widget.id_user.toString())
        .collection('user_professions')
        .add({'fk_id_profession':id_profession, 'status':true, 'createdAt': Timestamp.fromDate(DateTime.now())}).then((value) => {
          Navigator.pop(context)
    }).catchError((error) => print("Failed to add profession: $error"));
  }

}