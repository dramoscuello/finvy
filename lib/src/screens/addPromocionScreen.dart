import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:whereappv2/src/Widget/loadingWidget.dart';
import 'package:whereappv2/src/utils/TextStyles.dart';
import 'package:smart_select/smart_select.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';



class addpromocion extends StatefulWidget{
  String user_id;
  String bussines_id;
  String product_id;
  String price;

  addpromocion(this.user_id, this.bussines_id, this.product_id, this.price);

  @override
  addpromocionState createState() => addpromocionState();
}

class addpromocionState extends State<addpromocion>{

  List<Map<String,dynamic>> _listPromos = new List<Map<String,dynamic>>();
  List<Map<String,dynamic>> _listLocs = new List<Map<String,dynamic>>();
  String _promoSelected = '';
  List<String> _locSelected = [];

  TextEditingController txtPrice = TextEditingController();
  TextEditingController txtNewPrice = TextEditingController();
  TextEditingController txtPercent = TextEditingController();

  final GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();

  CollectionReference business = FirebaseFirestore.instance.collection('business');

  DateTime hoy = DateTime.now();

  String _date = '';
  DateTime picked;

  String _labelPromo = '';
  bool _loading = false;



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    txtPrice.text = widget.price;
    initializeDefault();
    getCategories();
    getLocations();
  }

  Future<void> initializeDefault() async {
    FirebaseApp app = await Firebase.initializeApp();
    assert(app != null);
    print('Initialized default app $app');
  }

  getCategories(){
    FirebaseFirestore.instance
        .collection('promotions')
        .get()
        .then((QuerySnapshot querySnapshot) => {
      setState(() {
        _listPromos.clear();
      }),
      querySnapshot.docs.forEach((doc) {
        setState(() {
          _listPromos.add({'value':doc['promotion_type'].toString(), 'title':doc['promotion']});
        });
      })
    });
  }

  getLocations(){
    business
        .doc(widget.bussines_id)
        .collection('locations_bussines')
        .get()
        .then((QuerySnapshot querySnapshot) => {
          setState(() {
            _listLocs.clear();
          }),
          querySnapshot.docs.forEach((doc) {
            setState(() {
              _listLocs.add({'value':doc.id, 'title':doc['address']});
            });
          })
    });
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        key: _scaffold,
        appBar: AppBar(
          title: NormalText("Agregar promoción", Colors.black, 19),
          centerTitle: true,
          leading: IconButton(icon: Icon(Icons.close), color:Colors.black ,onPressed: (){
            Navigator.pop(context);
          },),
          backgroundColor: Colors.white,
          actions: [
            IconButton(icon: Icon(Icons.check), color:Colors.black ,onPressed: (){
              submitValues();
            },)
          ],
        ),
        body: _loading?Center(child: CircularProgressIndicator()): Container(
            margin: EdgeInsets.fromLTRB(15,15,15,5),
            padding: EdgeInsets.fromLTRB(1,1,1,1),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  dropDownLoc(),
                  SizedBox(height: 10,
                    child: Divider(),
                  ),
                  dropDown(),
                  Visibility(
                    visible: (_promoSelected=='2' || _promoSelected == '3'),
                    child: entryPriceProduct(),
                  ),
                  Visibility(
                    visible: (_promoSelected == '2'),
                    child: entryNewPriceProduct(),
                  ),
                  Visibility(
                      visible: (_promoSelected=='2' || _promoSelected == '3'),
                      child: entryPercent()
                  ),
                  Visibility(
                      visible: true,
                      child: calendar()
                  ),
                ],
              ),
            )
        )
    );
  }

  Widget dropDown() {
    return Container(
      child: SmartSelect<String>.single(
          title: 'Tipos de promociones',
          placeholder: 'Escoger...',
          value: _promoSelected,
          onChange: (state) {
            setState((){
              _promoSelected = state.value;
            });
            _labelPromo = state.valueTitle;
          },
          choiceItems: S2Choice.listFrom<String, Map>(
            source: _listPromos,
            value: (index, item) => item['value'],
            title: (index, item) => item['title'],
          ),
          choiceGrouped: false,
          modalFilter: true,
          modalFilterAuto: true,
          modalFilterHint: 'Buscar...',
          tileBuilder: (context, state) {
            return S2Tile.fromState(
              state,
              isTwoLine: true,
              leading: Icon(FontAwesomeIcons.tags),
            );
          }
      ),
    );
  }


  Widget dropDownLoc(){
    return Container(
      child: SmartSelect<String>.multiple(
          title: 'Locales',
          placeholder: 'Escoger...',
          value: _locSelected,
          onChange: (state) => setState(() => _locSelected = state.value),
          choiceItems: S2Choice.listFrom<String, Map>(
            source: _listLocs,
            value: (index, item) => item['value'],
            title: (index, item) => item['title'],
          ),
          choiceGrouped: false,
          modalFilter: true,
          modalFilterAuto: true,
          modalFilterHint: 'Buscar...',
          modalFooterBuilder: (context, state) {
            return Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  ActionButton(
                    label: const Text('Marcar todos/Descarmar'),
                    onTap: () {
                      state.changes.selectToggle();
                    },
                  )
                ],
              ),
            );
          },
          tileBuilder: (context, state) {
            return S2Tile.fromState(
              state,
              isTwoLine: true,
              leading: Icon(FontAwesomeIcons.building),
            );
          }
      ),
    );
  }

  Widget entryPriceProduct() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText("Precio:",Colors.black, 16),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: txtPrice,
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.next,
            //maxLines: 3,
            decoration: InputDecoration(
              enabled: false,
                border: OutlineInputBorder(),
                fillColor: Color(0xffffffff),
                filled: true,
                prefixIcon: Icon(FontAwesomeIcons.moneyBill)
            ),)
        ],
      ),
    );
  }

  Widget entryNewPriceProduct() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText("Nuevo precio:",Colors.black, 16),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: txtNewPrice,
            keyboardType: TextInputType.numberWithOptions(),
            textInputAction: TextInputAction.next,
            //maxLines: 3,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                fillColor: Color(0xffffffff),
                filled: true,
                prefixIcon: Icon(FontAwesomeIcons.moneyBill)
            ),
            onChanged: (text){
              txtPercent.text = (((double.parse(text)/double.parse(txtPrice.text))-1)*-100).floor().toString();
            },
          )
        ],
      ),
    );
  }

  Widget entryPercent() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText("Porcentaje:",Colors.black, 16),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: txtPercent,
            keyboardType: TextInputType.numberWithOptions(),
            textInputAction: TextInputAction.next,
            decoration: InputDecoration(
                enabled: (_promoSelected == '3'),
                border: OutlineInputBorder(),
                fillColor: Color(0xffffffff),
                filled: true,
                prefixIcon: Icon(FontAwesomeIcons.percent)
            ),)
        ],
      ),
    );
  }

  Widget calendar(){
    return Container(
        margin: EdgeInsets.fromLTRB(0, 2, 0, 2),
        padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10.0),
          boxShadow: [
            BoxShadow(
              blurRadius: 10.0,
              color: Colors.grey[200],
            )
          ],
        ),
        child:ListTile(
            title: NormalText('Fecha',Color(0xff000000), 16),
            subtitle: NormalText(_date==''?'Escoger...':_date,Colors.grey, 15),
            leading: Icon(FontAwesomeIcons.calendarAlt),
            trailing: Icon(Icons.arrow_right),
            onTap: (){
              selectDate(context);
            }
        )
    );
  }

  void snackMsj(String msj, Color color){//funcion que muestra mensajes snack, aquello que se muestran en la parte inferior del dispositivo
    _scaffold.currentState.showSnackBar(SnackBar(content: NormalText(msj, Colors.white, 16),
        backgroundColor:color));
  }

  Future selectDate(BuildContext context)async{
    picked = await showDatePicker(context: context, initialDate: hoy, firstDate: hoy, lastDate: DateTime(2050));
    if(picked != null){
      setState(() {
        _date = picked.toString().substring(0,10);
      });
    }
  }

  submitValues(){
    if(_locSelected.length > 0){
      switch(_promoSelected){
        case '1':{
          if(picked != null){
            submitPromo();
          }else{
            snackMsj('Se debe seleccionar una fecha', Colors.red);
          }
        }
        break;
        case '2':{
          if(txtNewPrice.text != '' && picked != null){
            submitPromo();
          }else{
            snackMsj("Todos los campos son obligatorios", Colors.red);
          }
        }
        break;
        case '3':{
          if(txtPercent.text != '' && picked != null){
            submitPromo();
          }else{
            snackMsj("Todos los campos son obligatorios", Colors.red);
          }
        }
        break;
        default:{
          snackMsj('Se debe seleccionar una categoria', Colors.red);
        }
        break;
      }
    }else{
      snackMsj('Se debe seleccionar por lo menos una localización', Colors.red);
    }
  }

  submitPromo()async{

      setState(() {
        _loading = true;
      });

      for(var obj in _locSelected){
        await addPromo(obj);
      }
      Navigator.pop(context);
  }

  Future<void> addPromo(id_loc) {
    return business
        .doc(widget.bussines_id)
        .collection('products_bussines')
        .doc(widget.product_id.toString())
        .collection('promotions_products')
        .add({
      'promo':_labelPromo,
      'fk_id_location': id_loc,
      'percent': _promoSelected=='1'?'':txtPercent.text,
      'price': _promoSelected=='1'?'':txtPrice.text,
      'newPrice': _promoSelected=='2'?txtNewPrice.text:'',
      'date':Timestamp.fromDate(picked),
      'status':true
    }).then((value) => {
      print('OK')
    }).catchError((error) => print("Failed to add user: $error"));
  }

}

class ActionButton extends StatelessWidget {

  final Widget label;
  final VoidCallback onTap;

  ActionButton({
    Key key,
    this.label,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      child: label,
      color: Theme.of(context).primaryColor,
      textColor: Colors.white,
      onPressed: onTap,
    );
  }
}