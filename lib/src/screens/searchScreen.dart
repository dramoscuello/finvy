import 'dart:convert';
import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:whereappv2/src/Widget/loadingWidget.dart';
import 'package:whereappv2/src/Widget/widget404.dart';
import 'package:whereappv2/src/Widget/widgetError.dart';
import 'package:whereappv2/src/utils/TextStyles.dart';
import '../Widget/ProgresDialog.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';



class SearchScreen extends StatefulWidget{
  @override
  _State createState() => _State();
}

class _State extends State<SearchScreen>{
  bool isSearching = false;

  var showD = new showProgressDialog();
  final GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();

  var arraySearch = [];
  var arrayResults = [];

  bool panel1 = true;
  bool panel2 = false;

  bool robotSearch = false;

  AsyncMemoizer _memoizer = AsyncMemoizer();
  TextEditingController txtSearch = TextEditingController();



  get_services()async{
    return this._memoizer.runOnce(() async {
      await FirebaseFirestore.instance
          .collection('professions')
          .get()
          .then((QuerySnapshot querySnapshot) => {
            setState(() {
              arraySearch = querySnapshot.docs;
              arrayResults = querySnapshot.docs;
            })
        });

      return arraySearch;
    });
  }


  void ChangedTextSearch(String texto){
    //cuando cambia el contenido en el campo de texto de busqueda y si tiene mas de dos caracteres se hace la busqueda por tags y se actualiza la vista
    setState(() {
      arrayResults = [];
    });
    if(texto.length > 2){
      for(var i=0; i<arraySearch.length; i++){
        if(arraySearch[i]['tags'].toString().contains(texto)){
          setState(() {
            arrayResults.add(arraySearch[i]);
          });
        }
      }
      if(arrayResults.length == 0){
        setState(() {
          robotSearch = true;
        });
      }else{
        setState(() {
          robotSearch = false;
        });
      }
    }else{
      setState(() {
        arrayResults = arraySearch;
        robotSearch = false;
      });
    }
  }

  void BackAction(){
    if(isSearching){
      setState(() {
        isSearching = false;
        panel2 = false;
        panel1 = true;
      });
    }else{
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop:(){
      BackAction();
    },
      child:Scaffold(
        key: _scaffold,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor:isSearching?Color(0xffffffff):Color(0xff977EF2),
        leading: IconButton(icon:Icon(Icons.arrow_back, color: isSearching?Colors.black:Colors.white,),
            onPressed: (){Navigator.pop(context);}),
        title: isSearching?
        TextField(
          controller: txtSearch,
          autofocus: true,
          style:TextStyle(fontFamily: 'Amaranth', fontSize: 20, color: Colors.black),
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: "Buscar...",
            hintStyle: TextStyle(fontFamily: 'Amaranth', fontSize: 20, color: Colors.black),
          ),
          onChanged: (text){
            ChangedTextSearch(text);
          },
        ):
        NormalText("Buscar servicios...",Color(0xffffffff), 20),
        actions: <Widget>[
          IconButton(icon: isSearching?Icon(FontAwesomeIcons.times, color: Colors.black):Icon(FontAwesomeIcons.search),onPressed: (){ //CONDICIONAL BINARIO ?(SI), :(SINO)
            //SI LA VARIABLE isSearching ES TRUE SE MUESTRA EL ICONO CANCELAR, SINO, SE MUESTRA EL ICONO LUPA
            //AL HACER CLIC EN LA LUPA, SE CAMBIARA EL LABEL "BUSCAR SERVICIO" POR UN CAMPO DE TEXTO PARA REALIZAR LA BUSQUEDA
            setState(() {
              isSearching = !isSearching;
            });
            if(!isSearching){
              txtSearch.text = '';
              setState(() {
                robotSearch = false;
                arrayResults = arraySearch;
                panel1 = true;
                panel2 = false;
              });
            }else{
              panel1 = false;
              panel2 = true;
            }
          }
          )
        ],
      ),
        body: FutureWidgets()
      )
    );
  }

  Widget FutureWidgets(){
    return FutureBuilder(
        future: get_services(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if(snapshot.connectionState == ConnectionState.done) {
            if(snapshot.hasError){
              return WidgetError();
            }else if(snapshot.hasData){
              if(snapshot.data.length > 0){
                return widget_search(snapshot.data);
              }else{
                return Widget404();
              }
            }
          } else {
            return LoadingWidget(100, 100, 8);
          }
        }
    );
  }

  Widget widget_search(snapshot){
    return Stack(
      children: [
        Visibility(
          visible: panel1,
            child: ListView.separated(
                padding: EdgeInsets.fromLTRB(10.0,10.0,10.0,0),
                itemCount: snapshot.length,
                itemBuilder:(context, index){
                  return Container(
                      margin: EdgeInsets.fromLTRB(0, 2, 0, 2),
                      padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10.0),
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 10.0,
                            color: Colors.grey[350],
                          )
                        ],
                      ),
                      child:ListTile(
                        title: NormalText(snapshot[index]['profession'],Color(0xff000000), 18),
                        leading: Image.memory(Base64Decoder().convert(snapshot[index]['icon'].split(',').last), width: 40, height: 40),
                        trailing: Icon(Icons.playlist_add_check),
                        onTap: ()async{
                          setState(() {
                            isSearching = false;
                          });
                          await showD.showDialog(context, "Buscando ${snapshot[index]['profession']}...");
                          Future.delayed(Duration(seconds: 3)).then((value) async {
                            await showD.hideShowDialog();
                          });
                        },
                      )
                  );
                },
                separatorBuilder: (context, index){
                  return Divider();
                }

            )
        ),
        Visibility(
          visible: panel2,
            child: Stack(
              children: [
                Visibility(
                  visible: robotSearch,
                    child: Widget404()
                ),
                Visibility(
                  visible: true,
                    child: ListView.separated(
                        padding: EdgeInsets.fromLTRB(10.0,10.0,10.0,0),
                        itemCount: arrayResults.length,
                        itemBuilder:(context, index){
                          return Container(
                              margin: EdgeInsets.fromLTRB(0, 2, 0, 2),
                              padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10.0),
                                boxShadow: [
                                  BoxShadow(
                                    blurRadius: 10.0,
                                    color: Colors.grey[350],
                                  )
                                ],
                              ),
                              child:ListTile(
                                title: NormalText(arrayResults[index]['profession'],Color(0xff000000), 18),
                                leading: Image.memory(Base64Decoder().convert(arrayResults[index]['icon'].split(',').last), width: 40, height: 40),
                                trailing: Icon(Icons.playlist_add_check),
                                onTap: ()async{
                                  setState(() {
                                    isSearching = false;
                                  });
                                  await showD.showDialog(context, "Buscando ${arrayResults[index]['profession']}...");
                                  Future.delayed(Duration(seconds: 3)).then((value) async {
                                    await showD.hideShowDialog();
                                  });
                                },
                              )
                          );
                        },
                        separatorBuilder: (context, index){
                          return Divider();
                        }

                    )
                )
              ],
            )
        )
      ],
    );
  }
}