import 'package:flutter/material.dart';
import 'package:whereappv2/src/Widget/backButton.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:encrypt/encrypt.dart' as enk;
import 'package:whereappv2/src/config/sqlite_info_user.dart';
import 'package:whereappv2/src/utils/TextStyles.dart';

import 'package:email_validator/email_validator.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class editProfile extends StatefulWidget {
  String lbl_title;
  int id_user;
  int id_concepto;
  String concepto;
  String concepto2;
  String concepto3;
  String edit1;
  String edit2;
  String edit3;

  editProfile(this.lbl_title, this.id_user,  this.id_concepto, this.concepto, this.edit1, [this.concepto2 = "", this.edit2 = "" , this.concepto3 = "",  this.edit3=""]);
  @override
  editProfileState createState() => editProfileState();
}

class editProfileState extends State<editProfile> {

  editProfileState();


  var dbl = new ManageDb();


  bool visibleName = false;
  bool visibleUsername = false;
  bool visibleEmail = false;
  bool visibleTel = false;
  bool visiblePass = false;

  TextEditingController txtName = TextEditingController();
  TextEditingController txtLast = TextEditingController();
  TextEditingController txtUser = TextEditingController();
  TextEditingController txtEmail = TextEditingController();
  TextEditingController txtTel = TextEditingController();
  TextEditingController txtPass = TextEditingController();
  TextEditingController txtPass1 = TextEditingController();
  TextEditingController txtPass2 = TextEditingController();
  GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();

  //MENSAJES DE ERROR EN EL REGISTRO
  String errorUsername = '';
  String errorPass = '';
  String errorPass1 = '';
  String errorPass2 = '';
  String errorEmail = '';

  //PARAMETROS PARA ENCRIPTAR
  final key = enk.Key.fromUtf8('zxcvbnmasdfghjkl1234567890qwerty');
  final iv = enk.IV.fromLength(16);

  bool HidePass  = true;
  bool HidePass1 = true;
  bool HidePass2 = true;
  bool verifingUser = false;
  bool verifingEmail = false;
  bool isTypingUser = false;
  bool isTypingEmail = false;


  CollectionReference users = FirebaseFirestore.instance.collection('_users_services');




  void snackMsj(String msj){
    _scaffold.currentState.showSnackBar(SnackBar(content: Text(msj),
        backgroundColor:Colors.red));
  }

  Future<void> initializeDefault() async {
    FirebaseApp app = await Firebase.initializeApp();
    assert(app != null);
    print('Initialized default app $app');
  }

  @override
  void initState() {
    super.initState();

    initializeDefault();
    if(widget.id_concepto == 1){
      visibleName = true;
      txtName.text = widget.edit1;
      txtLast.text = widget.edit2;
    }else if(widget.id_concepto == 2){
      visibleUsername = true;
      txtUser.text = widget.edit1;
    }else if(widget.id_concepto == 3){
      visibleEmail = true;
      txtEmail.text = widget.edit1;
    }else if(widget.id_concepto == 4){
      visibleTel = true;
      txtTel.text = widget.edit1;
    }else if(widget.id_concepto == 5){
      visiblePass = true;
    }
  }


  Widget entryFieldName() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText(widget.concepto,Colors.black, 19),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: txtName,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true,
                prefixIcon: Icon(FontAwesomeIcons.list)
            ),)
        ],
      ),
    );
  }

  Widget entryFieldLast() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText(widget.concepto2,Colors.black, 19),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: txtLast,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true,
                prefixIcon: Icon(FontAwesomeIcons.list)
            ),)
        ],
      ),
    );
  }

  Widget entryFieldUsername() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText(widget.concepto,Colors.black, 19),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: txtUser,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true,
                prefixIcon: Icon(FontAwesomeIcons.list),
                suffixIcon: IconButton(onPressed: (){
                  print("OK");
                }
                ,icon: isTypingUser?verifingUser?CircularProgressIndicator():Icon(FontAwesomeIcons.check):Container()),
                errorText: errorUsername,
                errorStyle: TextStyle(
                    color: Colors.red,
                    fontSize: 14,
                    fontWeight: FontWeight.bold
                )
            ),
            autocorrect:false,
            onChanged: (text){
              checkUsername();
            },
          )
        ],
      ),
    );
  }

  Widget entryFieldEmail() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText(widget.concepto,Colors.black, 19),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: txtEmail,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true,
                prefixIcon: Icon(FontAwesomeIcons.list),
                suffixIcon: IconButton(onPressed: (){
                  print("OK");
                }
                    ,icon: isTypingEmail?verifingEmail?CircularProgressIndicator():Icon(FontAwesomeIcons.check):Container()),
                errorText: errorEmail,
                errorStyle: TextStyle(
                    color: Colors.red,
                    fontSize: 14,
                    fontWeight: FontWeight.bold
                )
            ),
            autocorrect:false,
            onChanged: (text){
              checkEmail();
            },
          )
        ],
      ),
    );
  }

  Widget entryFieldPass() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText(widget.concepto,Colors.black, 19),
          SizedBox(
            height: 10,
          ),
          TextField(
            obscureText: HidePass,
            controller: txtPass,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true,
                prefixIcon: Icon(FontAwesomeIcons.list),
              suffixIcon: IconButton(
                icon: HidePass?Icon(FontAwesomeIcons.eye):Icon(FontAwesomeIcons.eyeSlash),
                onPressed: (){
                  setState(() {
                    HidePass = !HidePass;
                  });
                },
              ),
                errorText: errorPass,
                errorStyle: TextStyle(
                    color: Colors.red,
                    fontSize: 14,
                    fontWeight: FontWeight.bold
                )
            ),
            autocorrect:false,

          )
        ],
      ),
    );
  }

  Widget entryFieldPass1() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText(widget.concepto2,Colors.black, 19),
          SizedBox(
            height: 10,
          ),
          TextField(
            obscureText: HidePass1,
            controller: txtPass1,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true,
                prefixIcon: Icon(FontAwesomeIcons.list),
                suffixIcon: IconButton(
                  icon: HidePass1?Icon(FontAwesomeIcons.eye):Icon(FontAwesomeIcons.eyeSlash),
                  onPressed: (){
                    setState(() {
                      HidePass1 = !HidePass1;
                    });
                  },
                ),
                errorText: errorPass1,
                errorStyle: TextStyle(
                    color: Colors.red,
                    fontSize: 14,
                    fontWeight: FontWeight.bold
                )
            ),
            autocorrect:false,
            onChanged: (text){
              checkPass1();
            },
          )
        ],
      ),
    );
  }

  Widget entryFieldPass2() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText(widget.concepto3,Colors.black, 19),
          SizedBox(
            height: 10,
          ),
          TextField(
            obscureText: HidePass2,
            controller: txtPass2,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true,
                prefixIcon: Icon(FontAwesomeIcons.list),
                suffixIcon: IconButton(
                  icon: HidePass2?Icon(FontAwesomeIcons.eye):Icon(FontAwesomeIcons.eyeSlash),
                  onPressed: (){
                    setState(() {
                      HidePass2 = !HidePass2;
                    });
                  },
                ),
                errorText: errorPass2,
                errorStyle: TextStyle(
                    color: Colors.red,
                    fontSize: 14,
                    fontWeight: FontWeight.bold
                )
            ),
            autocorrect:false,
            onChanged: (text){
              checkEqualsPass();
            },
          )
        ],
      ),
    );
  }

  Widget entryFieldTel() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText(widget.concepto,Colors.black, 19),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: txtTel,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true,
                prefixIcon: Icon(FontAwesomeIcons.list)
            ),)
        ],
      ),
    );
  }

  updateDataUser(int id, int idc, {String name="", String last="", String username="", String email="", String tel="", String pass="", String pass1 = "", String pass2=""})async{
    switch(idc){
      case 1:{
        return users
            .doc(id.toString())
            .update({'name':name, 'last_name':last})
            .then((value) => {
              dbl.UpdateNameYLast(id, name, last),
              ()async{
                await Navigator.pop(context);
              }()
            })
            .catchError((error) => print("Failed to update user: $error"));
      }
      break;
      case 2:{
        return users
            .doc(id.toString())
            .update({'username':username})
            .then((value) => {
              dbl.UpdateUsername(id, username),
              ()async{
                await Navigator.pop(context);
              }()
        })
            .catchError((error) => print("Failed to update user: $error"));
      }
      break;
      case 3:{
        return users
            .doc(id.toString())
            .update({'email':email})
            .then((value) => {
              dbl.UpdateEmail(id, email),
              ()async{
                await Navigator.pop(context);
              }()
        })
            .catchError((error) => print("Failed to update user: $error"));
      }
      break;
      case 4:{
        return users
            .doc(id.toString())
            .update({'phone':tel})
            .then((value) => {
              dbl.UpdateTel(id, tel),
              ()async{
                await Navigator.pop(context);
              }()
        })
            .catchError((error) => print("Failed to update user: $error"));
      }
      break;
      case 5:{
          showpass(id, pass, pass1, pass2);
      }
      break;
    }
  }

  showpass(id, pass, pass1, pass2)async{
    var oldpass = await dbl.get_pass();
    String oldpass_ =  oldpass[0]['password'];
    if(oldpass_ != pass){
      snackMsj("Contraseña actual incorrecta");
    }else{
      if(pass1 == pass2){
        return users
            .doc(id.toString())
            .update({'pass':pass1})
            .then((value) => {
              dbl.UpdatePass(id, pass1),
              ()async{
                await Navigator.pop(context);
              }()
            })
            .catchError((error) => print("Failed to update user: $error"));
      }
    }
  }


  Widget _submitButton() {
    return InkWell(
        onTap: () {
          switch(widget.id_concepto) {
            case 1: {
              if(txtName.text.trim() == "" || txtLast.text.trim() == ""){
                snackMsj("Los campos son obligatorios");
              }else{
                updateDataUser(widget.id_user, widget.id_concepto, name: txtName.text.trim(), last: txtLast.text.trim());
              }
            }
            break;
            case 2: {
              if(txtUser.text.trim() == ""){
                snackMsj("Los campos son obligatorios");
              }else{
                if(errorUsername == '' && !verifingUser){
                  updateDataUser(widget.id_user, widget.id_concepto, username: txtUser.text.trim());
                }
              }
            }
            break;
            case 3: {
              if(txtEmail.text.trim() == "" && !verifingEmail){
                snackMsj("Los campos son obligatorios");
              }else{
                if(errorEmail == ''){
                  updateDataUser(widget.id_user, widget.id_concepto, email: txtEmail.text.trim());
                }
              }
            }
            break;
            case 4: {
              if(txtTel.text.trim() == ""){
                snackMsj("Los campos son obligatorios");
              }else{
                updateDataUser(widget.id_user, widget.id_concepto, tel: txtTel.text.trim());
              }
            }
            break;
            case 5:{
              if(txtPass.text == "" || txtPass1.text == "" || txtPass2.text == ""){
                snackMsj("Los campos son obligatorios");
              }else{
                if(errorPass2 == '' && errorPass1 == ''){
                  final encrypter = enk.Encrypter(enk.AES(key));
                  final encrypted_pass = encrypter.encrypt(txtPass.text, iv: iv);
                  final encrypted_pass1 = encrypter.encrypt(txtPass1.text, iv: iv);
                  final encrypted_pass2 = encrypter.encrypt(txtPass2.text, iv: iv);
                  updateDataUser(widget.id_user, widget.id_concepto, pass: encrypted_pass.base64, pass1: encrypted_pass1.base64, pass2: encrypted_pass2.base64);
                }
              }
            }
            break;
          }
        },
        child:Container(
          width: MediaQuery.of(context).size.width/3,
          padding: EdgeInsets.symmetric(vertical: 5),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(45)),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.grey.shade200,
                    offset: Offset(2, 4),
                    blurRadius: 5,
                    spreadRadius: 2)
              ],
              gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [Color(0xff977EF2), Color(0xff2955D9)])),
          child: Icon(Icons.check, color: Colors.white, size: 40),
        )
    );
  }


  Widget _title() {
    return BoldText(widget.lbl_title, 25, Color(0xff000000));
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        key: _scaffold,
        body: SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height,
              child: Stack(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          flex: 3,
                          child: SizedBox(),
                        ),
                        _title(),
                        SizedBox(
                          height: 50,
                        ),
                        Visibility(
                            visible: visibleName,
                            child: entryFieldName()
                        ),
                        Visibility(
                            visible: visibleName,
                            child: entryFieldLast()
                        ),
                        Visibility(
                            visible: visibleUsername,
                            child: entryFieldUsername()
                        ),
                        Visibility(
                            visible: visibleEmail,
                            child: entryFieldEmail()
                        ),
                        Visibility(
                            visible: visibleTel,
                            child: entryFieldTel()
                        ),
                        Visibility(
                            visible: visiblePass,
                            child: entryFieldPass()
                        ),
                        Visibility(
                            visible: visiblePass,
                            child: entryFieldPass1()
                        ),
                        Visibility(
                            visible: visiblePass,
                            child: entryFieldPass2()
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        _submitButton(),
                        //_divider(),
                        Expanded(
                          flex: 2,
                          child: SizedBox(),
                        ),
                      ],
                    ),
                  ),
                  Positioned(top: 40, left: 0, child: backButton())
                ],
              ),
            )
        )
    );
  }

  checkEmail()async{
    bool isValid = EmailValidator.validate(txtEmail.text);
    if (isValid){
      setState(() {
        isTypingEmail = true;
        verifingEmail = true;
      });
      FirebaseFirestore.instance
          .collection('users')
          .where('email', isEqualTo:txtEmail.text.trim())
          .get()
          .then((QuerySnapshot querySnapshot) => {
        if(querySnapshot.docs.isEmpty){
          setState(() {
            errorEmail = '';
            verifingEmail = false;
          })
        }else{
          setState(() {
            errorEmail = 'Ya existe un registro con este Email';
            verifingEmail = true;
          })
        }
      })
          .catchError((error) => print("Failed: $error"));
    }else{
      setState(() {
        errorEmail = 'Email no valido';
        isTypingEmail = false;
        verifingEmail = false;
      });
    }
  }

  checkUsername()async{
    if(txtUser.text.length < 4){
      setState(() {
        errorUsername = 'Debe contener minimo 4 caracteres';
        isTypingUser = false;
        verifingUser = false;
      });
    }else{
      setState(() {
        isTypingUser = true;
        verifingUser = true;
      });

      FirebaseFirestore.instance
          .collection('users')
          .where('username', isEqualTo:txtUser.text.trim())
          .get()
          .then((QuerySnapshot querySnapshot) => {
        if(querySnapshot.docs.isEmpty){
          setState(() {
            errorUsername = '';
            verifingUser = false;
          })
        }else{
          setState(() {
            errorUsername = 'Ya existe un registro con este username';
            verifingUser = true;
          })
        }
      })
          .catchError((error) => print("Failed: $error"));
    }
  }

  void checkEqualsPass(){
    if(txtPass2.text !=  txtPass1.text){
      setState(() {
        errorPass2 = 'Las contraseñas no coinciden';
      });
    }else{
      setState(() {
        errorPass2 = '';
      });
    }
  }

  void checkPass1(){
    if(txtPass1.text.length < 8){
      setState(() {
        errorPass1 = 'La clave debe ser mayor o igual a 8 caracteres';
      });
    }else{
      setState(() {
        errorPass1 = '';
      });
      if(txtPass2.text.length > 1){
        checkEqualsPass();
      }
    }
  }


}
