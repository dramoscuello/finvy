import 'package:async/async.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:whereappv2/src/Widget/loadingWidget.dart';
import 'package:whereappv2/src/Widget/widget404.dart';
import 'package:whereappv2/src/Widget/widgetError.dart';
import 'package:whereappv2/src/screens/detailPriceScreen.dart';
import 'package:whereappv2/src/utils/TextStyles.dart';




class PricesServices extends StatefulWidget{
  String idProfUser;
  int user_id;
  PricesServices(this.idProfUser, this.user_id);

  @override
  PricesServicesState createState() => PricesServicesState();
}

class PricesServicesState extends State<PricesServices>{


  PricesServicesState();

  final GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();

  CollectionReference price;

  TextEditingController txtDesc = TextEditingController();
  TextEditingController txtPrice = TextEditingController();

  CollectionReference user = FirebaseFirestore.instance.collection('_users_services');


  deletePrice(String id){
    return price
        .doc(id)
        .delete()
        .then((value) => print("User deleted"))
        .catchError((error) => print("Failed to delete user: $error"));
  }

  @override
  void initState() {
    price = FirebaseFirestore.instance.collection('_users_services').doc(widget.user_id.toString()).collection('user_professions').doc(widget.idProfUser).collection('prices_profession');
    super.initState();
  }

  Future<void> showDialogDelete(id) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25.0))
          ),
          title: NormalText('¿Confirmar?',Colors.black, 22),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                NormalText('¡El elemento se eliminará!',Colors.black, 18),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: NormalText('Cancelar',Colors.lightBlue, 18),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: NormalText('OK',Colors.lightBlue, 18),
              onPressed: () {
                Navigator.of(context).pop();
                deletePrice(id);
              },
            )
          ],
        );
      },
    );
  }




  void snackMsj(String msj) {
    _scaffold.currentState.showSnackBar(SnackBar(content: NormalText(msj,Colors.white, 20),
        backgroundColor: Colors.red));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        backgroundColor: Colors.white,
        key: _scaffold,
        body:StreamBuilder(
            stream: price.snapshots(),
            builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
              if(!snapshot.hasData){
                return Center(
                  child: LoadingWidget(100, 100, 8),
                );
              }else{
                if(snapshot.data.docs.length > 0){
                  return prices(snapshot.data.docs);
                }else{
                  return Widget404();
                }
              }
            }
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => DetailPrice('Crear item', '', 0, null, widget.idProfUser, widget.user_id)));
          },
          label: Text("Añadir"),
          icon: Icon(Icons.add_circle),
        ),
    );
  }



  Widget prices(snapshot){
    return
      Stack(
        children: <Widget>[
          ListView.separated(
              padding: EdgeInsets.fromLTRB(10.0,10.0,10.0,0),
              itemCount: snapshot.length,
              itemBuilder:(context, index){
                return Container(
                    margin: EdgeInsets.fromLTRB(0, 2, 0, 2),
                    padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.0),
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 10.0,
                          color: Colors.grey[350],
                        )
                      ],
                    ),
                    child:ListTile(
                      title: NormalText(snapshot[index]['desc'],Color(0xff000000), 18),
                      leading: Icon(Icons.arrow_forward_ios),
                      subtitle: NormalText(snapshot[index]['price'].toString(),Colors.grey, 18),
                      onLongPress: (){
                        showSheetActions(context,'Actualizar item', snapshot[index]['desc'], snapshot[index]['price'], snapshot[index].id, widget.idProfUser, widget.user_id);
                      },

                    )
                );
              },
              separatorBuilder: (context, index){
                return Divider();
              }

          )
        ],
      );

  }

  showSheetActions(context, title, desc, price, item_id, prof_id, user_id){
    showModalBottomSheet<void>(
      context: context,
      builder: (context) {
        return Container(
            height: 120,
            color: Colors.white,
            child: ListView(
              itemExtent: 60,
              children: <Widget>[
                ListTile(
                  title: NormalText('Editar',Colors.black, 18),
                  leading: Icon(Icons.edit),
                  onTap: (){
                    Navigator.of(context).pop();
                    Navigator.push(context, MaterialPageRoute(builder: (context) => DetailPrice(title, desc, price, item_id, prof_id, user_id)));
                  },
                ),
                ListTile(
                  title: NormalText('Eliminar',Colors.black, 18),
                  leading: Icon(Icons.delete),
                  onTap: (){
                    Navigator.of(context).pop();
                    showDialogDelete(item_id);
                  },
                )
              ],
            )
        );
      },
    );
  }

}