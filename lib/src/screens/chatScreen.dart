import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


class chatScreen extends StatefulWidget{
  @override
  chatScreenState createState() => chatScreenState();
}

class chatScreenState extends State<chatScreen>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("My template flutter App"),
      ),
      body: StreamBuilder(
          stream: FirebaseFirestore.instance.collection('usuarios').snapshots(),
          builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
            if(!snapshot.hasData){
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            return ListView(
              children: snapshot.data.docs.map((document) {
                return Center(
                  child: Container(
                    width: MediaQuery.of(context).size.width / 1.2,
                    height: MediaQuery.of(context).size.height / 6,
                    child: Text("Title: " + document['id_user'].toString()),
                  ),
                );
              }).toList(),
            );
          }
      ),
    );
  }

}

/*
*
* Stack(
            children: <Widget>[
              GoogleMap(
                initialCameraPosition:CameraPosition(
                    target:currentLocation,
                    zoom: 16
                ),
                myLocationEnabled: locationEnabled,
                myLocationButtonEnabled: buttonLocationEnabled,
                mapType: MapType.normal,
                rotateGesturesEnabled: false,
                tiltGesturesEnabled: false,
                zoomControlsEnabled:false,
                onMapCreated: onMapcreate
              ),
              Positioned(
                left: 10,
                top: 20,
                child: IconButton(//icono que nos permitira mostrar el drawer menu de opciones
                  icon: Icon(FontAwesomeIcons.bars),
                  onPressed: (){
                    setMsjDrawer();
                    if(id_profile != null){
                      scaffoldKey.currentState.openDrawer();
                    }
                  },
                ),
              ),
            ],
          ),
*
* */