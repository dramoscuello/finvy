import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:whereappv2/src/Widget/widget404.dart';
import 'package:whereappv2/src/screens/addPromocionScreen.dart';
import 'package:whereappv2/src/utils/TextStyles.dart';



class promosBussines extends StatefulWidget{
  String user_id;
  String bussines_id;
  String product_id;
  String price;

  promosBussines(this.user_id, this.bussines_id, this.product_id, this.price);

  @override
  promosBussinesState createState() => promosBussinesState();
}

class promosBussinesState extends State<promosBussines>{

  CollectionReference business = FirebaseFirestore.instance.collection('business');

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: NormalText("Promociones", Colors.black, 19),
          centerTitle: true,
          leading: IconButton(icon: Icon(Icons.close), color:Colors.black ,onPressed: (){
            Navigator.pop(context);
          },),
          backgroundColor: Colors.white,
          actions: [
            IconButton(icon: Icon(Icons.add), color:Colors.black ,onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => addpromocion(widget.user_id, widget.bussines_id, widget.product_id, widget.price)));
            },)
          ],
        ),
        body: StreamBuilder(
            stream: business
                .doc(widget.bussines_id.toString())
                .collection('products_bussines')
                .doc(widget.product_id.toString())
                .collection('promotions_products')
                .snapshots(),
            builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
              if(!snapshot.hasData){
                return Center(
                  child: CircularProgressIndicator(),
                );
              }else{
                if(snapshot.data.docs.length > 0){
                  return ListView.separated(
                      padding: EdgeInsets.fromLTRB(10.0,10.0,10.0,0),
                      itemCount: snapshot.data.docs.length,
                      itemBuilder:(context, index){
                        return joinLocations(snapshot.data.docs[index]['fk_id_location'], index, snapshot.data.docs[index].id, snapshot.data.docs[index]);
                      },
                      separatorBuilder: (context, index){
                        return Divider();
                      }
                  );
                }else{
                  return Widget404();
                }
              }
            }
        )
    );
  }

  Widget joinLocations(idLocation, index, idProduct, info){
    return FutureBuilder<DocumentSnapshot>(
        future: business.doc(widget.bussines_id).collection('locations_bussines').doc(idLocation).get(),
        builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }else{
            return Container(
                margin: EdgeInsets.fromLTRB(0, 2, 0, 2),
                padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10.0),
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 10.0,
                      color: Colors.grey[350],
                    )
                  ],
                ),
                child:ListTile(
                  title: NormalText(info['promo'],Color(0xff000000), 18),
                  subtitle: NormalText(snapshot.data.data()['address'],Color(0xff000000), 16),
                  trailing: Icon(FontAwesomeIcons.tags),
                  onTap: (){
                    showSheetActions(context, idProduct);
                  }
                )
            );
          }
        }
    );
  }


  showSheetActions(BuildContext context, String id){
    showModalBottomSheet<void>(
      context: context,
      builder: (context) {
        return Container(
            height: 120,
            color: Colors.white,
            child: ListView(
              itemExtent: 60,
              children: <Widget>[
                ListTile(
                  title: NormalText('Editar',Colors.black, 18),
                  leading: Icon(FontAwesomeIcons.edit),
                  onTap: (){
                    Navigator.of(context).pop();
                  },
                ),
                ListTile(
                  title: NormalText('Eliminar',Colors.black, 18),
                  leading: Icon(FontAwesomeIcons.trash),
                  onTap: (){
                    Navigator.of(context).pop();
                    showDialogDelete(id);
                  },
                )
              ],
            )
        );
      },
    );
  }

  Future<void> showDialogDelete(String id_promo) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, 
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25.0))
          ),
          title: NormalText('¿Confirmar?',Colors.black, 22),

          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[

                NormalText('¡El elemento se eliminará!',Colors.black, 18),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: NormalText('Cancelar',Colors.lightBlue, 18),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: NormalText('OK',Colors.lightBlue, 18),
              onPressed: () {
                Navigator.of(context).pop();
                deletePromo(id_promo);
              },
            )
          ],
        );
      },
    );
  }
  
  deletePromo(String idPromo){
    return business
        .doc(widget.bussines_id.toString())
        .collection('products_bussines')
        .doc(widget.product_id)
        .collection('promotions_products')
        .doc(idPromo)
        .delete()
        .then((value) => print("promo deleted"))
        .catchError((error) => print("Failed to delete promo: $error"));
  }
  

}