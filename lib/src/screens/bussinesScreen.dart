import 'dart:convert';

import 'package:async/async.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:whereappv2/src/Widget/loadingWidget.dart';
import 'package:whereappv2/src/Widget/widget404.dart';
import 'package:whereappv2/src/Widget/widgetError.dart';
import 'package:whereappv2/src/config/sqlite_info_user.dart';
import 'package:whereappv2/src/screens/detailBussinesScreen.dart';
import 'package:whereappv2/src/utils/TextStyles.dart';

import 'addBussinesScreen.dart';


class BussinessScreen extends StatefulWidget{
  int id_user;
  bool accept;

  BussinessScreen([this.id_user, this.accept]);

  @override
  _StateBussiness createState() => _StateBussiness();
}

class _StateBussiness extends State<BussinessScreen> {

  _StateBussiness();

  bool check_accept = false;

  final GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();
  CollectionReference business = FirebaseFirestore.instance.collection('business');
  CollectionReference users = FirebaseFirestore.instance.collection('users');
  var managedb = new ManageDb();



  @override
  void initState() {
    super.initState();
  }


  void snackMsj(String msj) {
    _scaffold.currentState.showSnackBar(SnackBar(content: NormalText(msj,Color(0xffffffff), 18),
        backgroundColor: Colors.red));
  }


  @override
  Widget build(BuildContext context) {
    return toggle_widget();
  }

  Widget toggle_widget(){
    if(widget.accept){
      return widget_when_accept_terms();
    }else{
      return widget_no_terms();
    }
  }

  Widget widget_when_accept_terms(){
    return Scaffold(
        backgroundColor: Colors.white,
        key: _scaffold,
        appBar: AppBar(
          backgroundColor: Color(0xff977EF2),
          title: NormalText('Mis negocios',Colors.white, 20),
          actions: <Widget>[
            IconButton(icon: Icon(FontAwesomeIcons.plus, color: Colors.white,),
              tooltip: "Agregar servicio",
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => addBussiness(widget.id_user)));
              },
            )
          ],
        ),
        body:StreamBuilder(
            stream: business
                .where('fk_user_id', isEqualTo: widget.id_user)
                .snapshots(),
            builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
              if(!snapshot.hasData){
                return Center(
                  child: CircularProgressIndicator(),
                );
              }else{
                if(snapshot.data.docs.length > 0){
                  return widget_bussiness(snapshot.data.docs);
                }else{
                  return Widget404();
                }
              }
            }
        )
    );
  }



  Widget widget_bussiness(snapshot){
    return Stack(
      children: <Widget>[
        ListView.separated(
            padding: EdgeInsets.fromLTRB(10.0,10.0,10.0,0),
            itemCount: snapshot.length,
            itemBuilder:(context, index){
              return Container(
                  margin: EdgeInsets.fromLTRB(0, 2, 0, 2),
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10.0),
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 10.0,
                        color: Colors.grey[350],
                      )
                    ],
                  ),
                  child:ListTile(
                    title: NormalText(snapshot[index]['name'],Colors.black, 20),
                    leading: Image.network(snapshot[index]['logo'], width: 50, height: 50),
                    trailing: snapshot[index]['estado']?null:Icon(FontAwesomeIcons.toggleOff),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => detailBussines(snapshot[index], widget.id_user)));
                    },
                    onLongPress: (){
                      showSheetActions(context,!snapshot[index]['estado'], snapshot[index].id);
                    },
                  )
              );
            },
            separatorBuilder: (context, index){
              return Divider();
            }

        )
      ],
    );
  }

  showSheetActions(BuildContext context, bool estado, String id){
    showModalBottomSheet<void>(
      context: context,
      builder: (context) {
        return Container(
            height: 120,
            color: Colors.white,
            child: ListView(
              itemExtent: 60,
              children: <Widget>[
                ListTile(
                  title: NormalText(estado?'Habilitar':'Inhabilitar',Colors.black, 18),
                  leading: estado?Icon(FontAwesomeIcons.toggleOn):Icon(FontAwesomeIcons.toggleOff),
                  onTap: (){
                    Navigator.of(context).pop();
                    toggle_status(estado, id);
                  },
                ),
                ListTile(
                  title: NormalText('Eliminar',Colors.black, 18),
                  leading: Icon(Icons.delete),
                  onTap: (){
                    Navigator.of(context).pop();
                    showDialogDelete(id);
                  },
                )
              ],
            )
        );
      },
    );
  }

  Widget widget_no_terms(){
    return Scaffold(
      key: _scaffold,
      backgroundColor: Colors.white,
      body: Container(
        child: Center(
            child: Wrap(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Container(
                        child: Image(
                          image: AssetImage('resources/images/terms.png'),
                          height: 300,
                          width: 280,
                        )
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 55, vertical: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Checkbox(
                              value: check_accept,
                              onChanged: (value){
                                setState(() {
                                  check_accept = !check_accept;
                                });
                              }),
                          Expanded(
                              child: NormalText("Aceptar terminos y restricciones",Color(0xff2955D9), 18),
                          ),
                        ],
                      ),
                    ),
                    btn_accept()
                  ],
                ),
                //Positioned(top: 40, left: 0, child: backButton())
              ],
            )
        ),
      ),
    );
  }

  Widget btn_accept() {
    return InkWell(
        onTap: () {
          if(check_accept){
            Accepted();
          }else{
            snackMsj("¡Debe aceptar los terminos y restricciones!");
          }
        },
        child:Container(
          width: MediaQuery.of(context).size.width/2,
          padding: EdgeInsets.symmetric(vertical: 15),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(45)),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.grey.shade200,
                    offset: Offset(2, 4),
                    blurRadius: 5,
                    spreadRadius: 2)
              ],
              gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [Color(0xff977EF2), Color(0xff2955D9)])),
          child: NormalText("Aceptar",Color(0xffffffff), 22),
        )
    );
  }

  Future Accepted(){
    return users
        .doc(widget.id_user.toString())
        .update({'accept_terms_bus':true})
        .then((value) => {
      //managedb.UpdateTermsBus(widget.id_user, 'true'),
      setState(() {
        widget.accept = true;
      }),
    })
        .catchError((error) => print("Failed to update user: $error"));
  }


  toggle_status(bool status, String ids){
    return business
        .doc(ids)
        .update({'estado': status})
        .then((value) => print("Bussines Updated"))
        .catchError((error) => print("Failed to update Bussines: $error"));
  }

  Future<void> showDialogDelete(String id) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25.0))
          ),
          title: NormalText('¿Confirmar?',Colors.black, 22),

          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[

                NormalText('¡El elemento se eliminará!',Colors.black, 18),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: NormalText('Cancelar',Colors.lightBlue, 18),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: NormalText('OK',Colors.lightBlue, 18),
              onPressed: () {
                Navigator.of(context).pop();
                delete_bussines(id);
              },
            )
          ],
        );
      },
    );
  }

  delete_bussines(String id){
    return business
        .doc(id)
        .delete()
        .then((value) => print("User deleted"))
        .catchError((error) => print("Failed to delete user: $error"));
  }

}


