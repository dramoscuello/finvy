import 'dart:convert';
import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:whereappv2/src/Widget/loadingWidget.dart';
import 'package:whereappv2/src/Widget/widget404.dart';
import 'package:whereappv2/src/Widget/widgetError.dart';
import 'package:whereappv2/src/config/sqlite_info_user.dart';
import 'package:whereappv2/src/screens/addServiceScreen.dart';
import 'package:whereappv2/src/utils/TextStyles.dart';
import '../Widget/ProgresDialog.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'detailServiceScreen.dart';




class ServiceScreen extends StatefulWidget{
  int id_user;
  int nservices;
  bool accept;
  ServiceScreen([this.id_user, this.nservices, this.accept]);


  @override
  _StateService createState() => _StateService();
}

class _StateService extends State<ServiceScreen> {


  _StateService();


  var arrayItems = [];
  var arraySearch = [];
  var showD = new showProgressDialog();
  final GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();

  bool check_accept = false;

  CollectionReference users = FirebaseFirestore.instance.collection('_users_services');
  var managedb = new ManageDb();
  List<dynamic> _professions = new List<dynamic>();




  populateList()async{
    await FirebaseFirestore.instance
        .collection('professions')
        .where('status', isEqualTo:true)
        .get()
        .then((QuerySnapshot querySnapshot) => {
          arraySearch.length = 0,
            for(int i=0; i<querySnapshot.docs.length; i++){
                if(!_professions.any((element) => element['fk_id_profession'] == querySnapshot.docs[i].id)){
                  arraySearch.add(querySnapshot.docs[i]),
                }
            }
    });
    await Navigator.push(context, MaterialPageRoute(builder: (context) => addService(arraySearch, widget.id_user)));
  }


  void snackMsj(String msj) {
    _scaffold.currentState.showSnackBar(SnackBar(content: NormalText(msj,Colors.white, 16),
        backgroundColor: Colors.red));
  }

  @override
  Widget build(BuildContext context) {
    return toggle_widget();
  }

  @override
  void initState() {
    initializeDefault();
  }

  Future<void> initializeDefault() async {
    FirebaseApp app = await Firebase.initializeApp();
    assert(app != null);
    print('Initialized default app $app');
  }

  toggle_status(bool status, String ids){
    return users
        .doc(widget.id_user.toString())
        .collection('user_professions')
        .doc(ids)
        .update({'status': status})
        .then((value) => print("Status Updated"))
        .catchError((error) => print("Failed to update status: $error"));
  }

  Widget toggle_widget(){
    if(widget.accept){
      return widget_when_accept_terms();
    }else{
      return widget_no_terms();
    }
  }

  Widget widget_when_accept_terms(){
    return Scaffold(
        backgroundColor: Colors.white,
        key: _scaffold,
        appBar: AppBar(
          backgroundColor: Color(0xff977EF2),
          title:NormalText('Mis servicios',Colors.white, 20),
          actions: <Widget>[
            IconButton(icon: Icon(FontAwesomeIcons.plus, color: Colors.white,),
              tooltip: "Agregar servicio",
              onPressed: () {
                populateList();
              },
            )
          ],
        ),
        body:StreamBuilder(
            stream: users
                .doc(widget.id_user.toString())
                .collection('user_professions')
                .snapshots(),
            builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
              if(!snapshot.hasData){
                return Center(
                  child: CircularProgressIndicator(),
                );
              }else{
                _professions.length = 0;
                _professions = snapshot.data.docs;
                if(_professions.length > 0){
                  return ListView.separated(
                      padding: EdgeInsets.fromLTRB(10.0,10.0,10.0,0),
                      itemCount: snapshot.data.docs.length,
                      itemBuilder:(context, index){
                        return joinProfessions(_professions[index]['fk_id_profession'], index, _professions[index].id, _professions[index]['status']);
                      },
                      separatorBuilder: (context, index){
                        return Divider();
                      }
                  );
                }else{
                  return Widget404();
                }
              }
            }
        )
    );
  }

  Widget joinProfessions(String id, int index, String idProfUser, bool status){
    return FutureBuilder<DocumentSnapshot>(
        future: FirebaseFirestore.instance.collection('professions').doc(id).get(),
        builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }else{
            return Container(
                margin: EdgeInsets.fromLTRB(0, 2, 0, 2),
                padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10.0),
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 10.0,
                      color: Colors.grey[350],
                    )
                  ],
                ),
                child:ListTile(
                  title: NormalText(snapshot.data.data()['profession'],Color(0xff000000), 18),
                  leading: Image.memory(Base64Decoder().convert(snapshot.data.data()['icon'].split(',').last), width: 40, height: 40),
                  trailing: status?null:Icon(FontAwesomeIcons.toggleOff),
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => DetailService(idProfUser, snapshot.data.data()['profession'], widget.id_user)));
                  },
                  onLongPress: (){
                    showSheetActions(context, !status, idProfUser);
                  },
                )
            );
          }
        }
    );
  }

  showSheetActions(BuildContext context, bool status, String id){
    showModalBottomSheet<void>(
      context: context,
      builder: (context) {
        return Container(
            height: 120,
            color: Colors.white,
            child: ListView(
              itemExtent: 60,
              children: <Widget>[
                ListTile(
                  title: NormalText(status?'Habilitar':'Inhabilitar',Colors.black, 18),
                  leading: status?Icon(FontAwesomeIcons.toggleOn):Icon(FontAwesomeIcons.toggleOff),
                  onTap: (){
                    Navigator.of(context).pop();
                    toggle_status(status, id);
                  },
                ),
                ListTile(
                  title: NormalText('Eliminar',Colors.black, 18),
                  leading: Icon(Icons.delete),
                  onTap: (){
                    Navigator.of(context).pop();
                    showDialogDelete(id);
                  },
                )
              ],
            )
        );
      },
    );
  }



  delete_perfil_profesion(String id_profesion){    
    return users
        .doc(widget.id_user.toString())
        .collection('user_professions')
        .doc(id_profesion)
        .delete()
        .then((value) => print("Profession deleted"))
        .catchError((error) => print("Failed to delete Profession: $error"));
  }

  Future<void> showDialogDelete(String id_profesion) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25.0))
          ),
          title: NormalText('¿Confirmar?',Colors.black, 22),

          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[

                NormalText('¡El elemento se eliminará!',Colors.black, 18),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: NormalText('Cancelar',Colors.lightBlue, 18),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: NormalText('OK',Colors.lightBlue, 18),
              onPressed: () {
                Navigator.of(context).pop();
                delete_perfil_profesion(id_profesion);
              },
            )
          ],
        );
      },
    );
  }

  Widget widget_no_terms(){
    return Scaffold(
      key: _scaffold,
      backgroundColor: Colors.white,
      body: Container(
        child: Center(
            child: Wrap(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Container(
                        child: Image(
                          image: AssetImage('resources/images/terms.png'),
                          height: 300,
                          width: 280,
                        )
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 55, vertical: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Checkbox(
                              value: check_accept,
                              onChanged: (value){
                                setState(() {
                                  check_accept = !check_accept;
                                });
                              }),
                          Expanded(
                              child: NormalText("Aceptar terminos y restricciones",Color(0xff2955D9), 18),
                          ),
                        ],
                      ),
                    ),
                    btn_accept()
                  ],
                ),
                //Positioned(top: 40, left: 0, child: backButton())
              ],
            )
        ),
      ),
    );
  }


  Widget btn_accept() {
    return InkWell(
        onTap: () {
          if(check_accept){
            Accepted();
          }else{
            snackMsj("¡Debe aceptar los terminos y restricciones!");
          }
        },
        child:Container(
          width: MediaQuery.of(context).size.width/2,
          padding: EdgeInsets.symmetric(vertical: 15),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(45)),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.grey.shade200,
                    offset: Offset(2, 4),
                    blurRadius: 5,
                    spreadRadius: 2)
              ],
              gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [Color(0xff977EF2), Color(0xff2955D9)])),
          child: NormalText("Aceptar",Color(0xffffffff), 22),
        )
    );
  }


  Future Accepted(){
    return users
        .doc(widget.id_user.toString())
        .update({'accept_terms_serv':true})
        .then((value) => {
          managedb.UpdateTermsServ(widget.id_user, 'true'),
          setState(() {
            widget.accept = true;
          }),
        })
        .catchError((error) => print("Failed to update user: $error"));
  }

}




