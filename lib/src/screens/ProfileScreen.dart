import 'dart:async';
import 'dart:io';
import 'dart:ui';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:uuid/uuid.dart';
import 'package:whereappv2/src/Widget/loadingWidget.dart';
import 'package:whereappv2/src/config/sqlite_info_user.dart';
import 'package:whereappv2/src/screens/editProfileScreen.dart';
import 'package:whereappv2/src/screens/viewPhotoScreen.dart';
import 'package:whereappv2/src/utils/TextStyles.dart';

import 'package:image_cropper/image_cropper.dart';
import 'package:file_picker/file_picker.dart';
import 'dart:convert';
import 'package:whereappv2/src/utils/profile_user_default.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ProfileScreen extends StatefulWidget {
  int id_user;
  ProfileScreen(this.id_user);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<ProfileScreen> {


  _ProfileState();

  bool estadoCuentaVisible = true;
  bool visiblePrestadorInfo = true;
  bool visibleLoading = false;

  final GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();

  var dbl = new ManageDb();

  bool EditDate = false;
  String dateborn;

  DateTime hoy = DateTime.now();
  CollectionReference users = FirebaseFirestore.instance.collection('_users_services');
  File imageFile;



  @override
  void initState() {
    super.initState();
    initializeDefault();
  }

  Future selectDate(BuildContext context)async{
    final DateTime picked = await showDatePicker(context: context, initialDate: hoy, firstDate: DateTime(1950), lastDate: hoy);
    if(picked != null){
      dateborn = picked.toString().substring(0,10);
    }else{
      setState(() {
        EditDate = !EditDate;
      });
    }
  }

  Widget ListInfo(snapshot){
    return ListView(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            InkWell(
              onTap: () {
                showSheetActions(context, snapshot['picture']);
              },
              child: Padding(
                padding: EdgeInsets.only(left: 10.0, right: 10.0),
                child:Center(
                  child: Stack(
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(50),
                        child:  Image.network(snapshot['picture'], fit: BoxFit.cover, width: 100.0, height: 100.0),
                      ),
                      Visibility(
                        visible: visibleLoading,
                        child: Padding(
                          padding: EdgeInsets.all(25.0),
                          child: LoadingWidget(50, 50, 5),
                        )
                      )

                    ],
                  )
                ),
              ),
            ),

            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Visibility(
                    visible:visiblePrestadorInfo,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        NormalText("Trabajos realizados: ${snapshot['njobdone']}",Color(0xff000000), 18),
                        SizedBox(height: 5.0),
                      ],
                    ),
                  ),

                  Visibility(
                    visible: visiblePrestadorInfo,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        NormalText("Reputación: ${snapshot['reputation']}",Color(0xff000000), 18),
                        SizedBox(height: 5.0),
                      ],
                    ),
                  ),
                  Visibility(
                    visible: estadoCuentaVisible,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        NormalText("Cuenta: Sin confirmar",Colors.blueAccent, 18),
                      ],
                    ),
                  )
                ],
              ),
              flex: 3,
            ),
          ],
        ),
        Divider(),
        Container(height: 15.0),
        Padding(
          padding: EdgeInsets.all(5.0),
          child:
          NormalText("Información de la cuenta".toUpperCase(),Colors.black, 19),
        ),
        ListTile(
          title:
          NormalText("${snapshot['name']} ${snapshot['last_name']}",Colors.black, 17),
          subtitle: NormalText("Nombres y apellidos",Colors.grey, 16),
          leading: Icon(FontAwesomeIcons.addressCard),
          trailing: IconButton(onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => editProfile('Editar Nombres y apellidos', widget.id_user, 1, "Nombres", "${snapshot['name']}", "Apellidos", "${snapshot['last_name']}")));
          },icon:Icon(Icons.edit),
            tooltip: "Editar",
          ),

        ),
        ListTile(

          title:  NormalText("${snapshot['username']}",Colors.black, 17),
          subtitle: NormalText("Username",Colors.grey, 16),
          leading: Icon(FontAwesomeIcons.user),
          trailing: IconButton(onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => editProfile('Editar username', widget.id_user, 2, "Username:", "${snapshot['username']}")));
          },icon:Icon(Icons.edit),
            tooltip: "Editar",
          ),
        ),
        ListTile(
          title:

          NormalText("${snapshot['email']}",Colors.black, 17),
          subtitle: NormalText("Email",Colors.grey, 16),
          leading: Icon(FontAwesomeIcons.envelope),
          trailing: IconButton(onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => editProfile('Editar e-mail', widget.id_user, 3, "E-mail:","${snapshot['email']}")));
            },icon:Icon(Icons.edit),
            tooltip: "Editar",
          ),
        ),

        ListTile(
          title:
          NormalText("${snapshot['phone']!=''?snapshot['phone']:'No hay registro'}",Colors.black, 17),
          subtitle: NormalText("Telefono",Colors.grey, 16),
          leading: Icon(FontAwesomeIcons.phone),
          trailing: IconButton(onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => editProfile('Editar telefono', widget.id_user, 4, "Telefono:","${snapshot['phone']}")));
          },icon:Icon(Icons.edit),
            tooltip: "Editar",
          ),

        ),


        ListTile(
          title:
          NormalText("${snapshot['date_born']!=''?snapshot['date_born'].toString().substring(0,10):'No hay registro'}",Colors.black, 17),

          subtitle: NormalText("Fecha de nacimiento",Colors.grey, 16),
            leading: Icon(FontAwesomeIcons.calendarAlt),
          trailing: IconButton(onPressed: (){
            if(EditDate == false){
              selectDate(context);
            }else{
              if(dateborn != null){
                actualizarFecha(dateborn);
              }
            }
            setState(() {
              EditDate = !EditDate;
            });
          },icon:EditDate?Icon(FontAwesomeIcons.check, color: Colors.red,):Icon(Icons.edit),
            tooltip: EditDate?"Actualizar":"Editar",
          )
        ),
        ListTile(
          title:
          NormalText("********",Colors.black, 17),
          subtitle: NormalText("Contraseña",Colors.grey, 16),
          leading: Icon(FontAwesomeIcons.key),
          trailing: IconButton(onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => editProfile('Editar contraseña', widget.id_user, 5, "Contraseña actual", "", "Nueva contraseña", "", "Repetir contraseña nueva", "")));
          }
              ,icon:Icon(Icons.edit)),
        ),
      ],
    );
  }

  actualizarFecha(String fecha)async{
    return users
        .doc(widget.id_user.toString())
        .update({'date_born':fecha})
        .then((value) => {
          dbl.UpdateDateBorn(widget.id_user, fecha)
        })
        .catchError((error) => print("Failed to update user: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      key: _scaffold,
      appBar: AppBar(
        backgroundColor:Color(0xff977EF2),
        title: NormalText("Identificación: ${widget.id_user}",Colors.white, 20)
      ),
      body: Container(
        padding: EdgeInsets.fromLTRB(10.0,10.0,10.0,0),
        child: StreamBuilder(
            stream: users
                .doc(widget.id_user.toString())
                .snapshots(),
            builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot){
              if(!snapshot.hasData){
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
              return ListInfo(snapshot.data.data());
            }
        ),
      ),
    );
  }



  showSheetActions(BuildContext context, photo){
    showModalBottomSheet<void>(
      context: context,
      builder: (context) {
        return Container(
            height: 180,
            color: Colors.white,
            child: ListView(
              itemExtent: 60,
              children: <Widget>[
                ListTile(
                  title: NormalText('Ver',Colors.black, 18),
                  leading: Icon(Icons.remove_red_eye),
                  onTap: (){
                    Navigator.of(context).pop();
                    Navigator.push(context, MaterialPageRoute(builder: (context) => ViewPhotoScreen([{'id':0, 'photo':photo, 'caption':'Picture'}], 0)));
                  },
                ),
                ListTile(
                  title: NormalText('Subir',Colors.black, 18),
                  leading: Icon(Icons.cloud_upload),
                  onTap: (){
                    Navigator.of(context).pop();
                    _pickImage();
                    },
                ),
                ListTile(
                  title: NormalText('Eliminar',Colors.black, 18),
                  leading: Icon(Icons.delete),
                  onTap: (){
                    Navigator.of(context).pop();
                    DeletePicProfile();
                  },
                )
              ],
            )
        );
      },
    );
  }

  Future<Null> _pickImage() async {
    imageFile = await FilePicker.getFile(type: FileType.image);
    if (imageFile != null) {
      _cropImage();
    }
  }

  Future<Null> _cropImage() async {
    File croppedFile = await ImageCropper.cropImage(
        sourcePath: imageFile.path,
        aspectRatioPresets: [CropAspectRatioPreset.square],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Recortar imagen',
            toolbarColor: Color(0xff977EF2),
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          title: 'Recortar imagen',
        ));
    if (croppedFile != null) {
      setState(() {
        visibleLoading = true;
        imageFile = croppedFile;
      });
      ConvertImageToBase64(croppedFile);
    }
  }

  void ConvertImageToBase64(File image)async{
    List<int> imageBytes = await image.readAsBytesSync();
    String base64Image = await base64Encode(imageBytes);

    var nameImage = Uuid().v1();
    var PathImage = "/users/${widget.id_user}/profile_pic/$nameImage.jpg";
    final StorageReference storageReference = FirebaseStorage().ref().child(PathImage);

    final StorageUploadTask uploadTask = storageReference.putData(imageBytes);

    final StreamSubscription<StorageTaskEvent> streamSubscription = uploadTask.events.listen((event) {
      print('EVENT ${event.type}');
    });

    await uploadTask.onComplete;
    streamSubscription.cancel();

    var image_profile = (await storageReference.getDownloadURL()).toString();
    updatePicture(image_profile, widget.id_user);
  }

  Future<void> initializeDefault() async {
    FirebaseApp app = await Firebase.initializeApp();
    assert(app != null);
    print('Initialized default app $app');
  }

  updatePicture(img, id){
    return users
        .doc(id.toString())
        .update({'picture':img})
        .then((value) => {
          setState(() {
            visibleLoading = false;
          }),
          dbl.UpdatePicture(id, img)
        })
        .catchError((error) => print("Failed to update user: $error"));
  }

  DeletePicProfile(){
    return users
      .doc(widget.id_user.toString())
      .update({'picture':pic_default})
      .then((value) => {
        dbl.UpdatePicture( widget.id_user, pic_default)
      })
      .catchError((error) => print("Failed to update user: $error"));
  }
}
