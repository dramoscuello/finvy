import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:uuid/uuid.dart';
import 'package:whereappv2/src/Widget/loadingWidget.dart';
import 'package:whereappv2/src/screens/addressBussinesScreen.dart';
import 'package:whereappv2/src/screens/horariosScreen.dart';
import 'package:whereappv2/src/screens/productsBussinesScreen.dart';
import 'package:whereappv2/src/screens/promocionesBussinesScreen.dart';
import 'package:whereappv2/src/screens/viewPhotoScreen.dart';
import 'package:whereappv2/src/utils/TextStyles.dart';
import 'package:whereappv2/src/utils/profile_user_default.dart';


class detailBussines extends StatefulWidget {
  var info;
  var user_id;
  detailBussines(this.info, this.user_id);

  @override
  detailBussinesState createState() => detailBussinesState();
}

class detailBussinesState extends State<detailBussines> {


  detailBussinesState();


  bool visibleLoading = false;
  String Newpic;
  String _name;
  String _desc;
  String _tel;
  String _web;
  String _email;
  File imageFile;


  final GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();
  CollectionReference business = FirebaseFirestore.instance.collection('business');
  TextEditingController txtEdit = TextEditingController();



  @override
  void initState() {
    super.initState();
    initializeDefault();
  }


  Widget ListInfo(){
    return ListView(
      children: <Widget>[
          Center(
            child: Container(
              margin: EdgeInsets.all(10),
              child: InkWell(
                onTap: () {
                  showSheetActions(context, Newpic==null?widget.info['logo']:Newpic, widget.info.id);
                },
                child: Padding(
                  padding: EdgeInsets.only(left: 10.0, right: 10.0),
                  child:Center(
                      child: Stack(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(50),
                            child:  Image.network(Newpic==null?widget.info['logo']:Newpic, fit: BoxFit.cover, width: 100.0, height: 100.0),
                          ),
                          Visibility(
                              visible: visibleLoading,
                              child: Padding(
                                padding: EdgeInsets.all(25.0),
                                child: LoadingWidget(50, 50, 5),
                              )
                          )
                        ],
                      )
                  ),
                ),
              ),
            )
          ),
        Divider(),
        ListTile(
          title:
          NormalText(_name==null?widget.info['name']:_name,Colors.black, 17),
          subtitle: NormalText("Nombre del negocio",Colors.grey, 16),
          leading: Icon(FontAwesomeIcons.building),
          trailing: IconButton(onPressed: (){
            showSheetActionsEdit(context, 'Nombre del negocio', _name==null?widget.info['name']:_name, 'name', widget.info.id, TextInputType.text,1);
          },icon:Icon(Icons.edit),
            tooltip: "Editar",
          ),

        ),
        ListTile(
          title:  NormalText(_desc==null?widget.info['descripcion']:_desc,Colors.black, 17),
          subtitle: NormalText("Descripción",Colors.grey, 16),
          leading: Icon(FontAwesomeIcons.info),
          trailing: IconButton(onPressed: (){
            showSheetActionsEdit(context, 'Descripción', _desc==null?widget.info['descripcion']:_desc, 'descripcion', widget.info.id, TextInputType.text,2);
          },icon:Icon(Icons.edit),
            tooltip: "Editar",
          ),
        ),
        ListTile(
          title:
          NormalText(_tel==null?widget.info['phone']:_tel,Colors.black, 17),
          subtitle: NormalText("Teléfono",Colors.grey, 16),
          leading: Icon(FontAwesomeIcons.phone),
          trailing: IconButton(onPressed: (){
            showSheetActionsEdit(context, 'Teléfono', _tel==null?widget.info['phone']:_tel, 'phone', widget.info.id, TextInputType.phone,3);
          },icon:Icon(Icons.edit),
            tooltip: "Editar",
          ),
        ),
        ListTile(
            title:
            NormalText(_web==null?widget.info['website']!=''?widget.info['website']:'No hay información':_web,Colors.black, 17),
            subtitle: NormalText("Sitio web",Colors.grey, 16),
            leading: Icon(FontAwesomeIcons.laptopCode),
            trailing: IconButton(onPressed: (){
              showSheetActionsEdit(context, 'Sitio web', _web==null?widget.info['website']:_web, 'website', widget.info.id, TextInputType.url,4);
            },icon:Icon(Icons.edit),
              tooltip: "Editar",
            )
        ),
        ListTile(
          title:
          NormalText(_email==null?widget.info['email']!=''?widget.info['email']:'No hay información':_email,Colors.black, 17),
          subtitle: NormalText("E-mail",Colors.grey, 16),
          leading: Icon(FontAwesomeIcons.envelope),
          trailing: IconButton(onPressed: (){
            showSheetActionsEdit(context, 'E-mail', _email==null?widget.info['email']:_email, 'email', widget.info.id, TextInputType.emailAddress,5);
          }
              ,icon:Icon(Icons.edit)),
        ),
        ListTile(
          title:  NormalText('Direcciones',Colors.black, 17),
          subtitle: NormalText("Ver",Colors.grey, 16),
          leading: Icon(FontAwesomeIcons.mapMarkedAlt),
          trailing: IconButton(onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => addressBussines(widget.user_id.toString(), widget.info.id.toString())));
          },icon:Icon(Icons.remove_red_eye),
            tooltip: "Ver",
          ),
        ),
        ListTile(
          title:  NormalText('Productos',Colors.black, 17),
          subtitle: NormalText("Ver",Colors.grey, 16),
          leading: Icon(FontAwesomeIcons.list),
          trailing: IconButton(onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => productsBussines(widget.user_id.toString(), widget.info.id.toString())));
          },icon:Icon(Icons.remove_red_eye),
            tooltip: "Ver",
          ),
        )
      ],
    );
  }

  Future<void> initializeDefault() async {
    FirebaseApp app = await Firebase.initializeApp();
    assert(app != null);
    print('Initialized default app $app');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      key: _scaffold,
      appBar: AppBar(
          backgroundColor:Color(0xff977EF2),
          title: NormalText("Nit: ${widget.info.id}",Colors.white, 20)
      ),
      body: ListInfo(),
    );
  }

  showSheetActions(BuildContext context, photo, id_bussines){
    showModalBottomSheet<void>(
      context: context,
      builder: (context) {
        return Container(
            height: 180,
            color: Colors.white,
            child: ListView(
              itemExtent: 60,
              children: <Widget>[
                ListTile(
                  title: NormalText('Ver',Colors.black, 18),
                  leading: Icon(Icons.remove_red_eye),
                  onTap: (){
                    Navigator.of(context).pop();
                    Navigator.push(context, MaterialPageRoute(builder: (context) => ViewPhotoScreen([{'id':0, 'photo':photo, 'caption':'Picture'}], 0)));
                  },
                ),
                ListTile(
                  title: NormalText('Subir',Colors.black, 18),
                  leading: Icon(Icons.cloud_upload),
                  onTap: (){
                    Navigator.of(context).pop();
                    _pickImage();
                  },
                ),
                ListTile(
                  title: NormalText('Eliminar',Colors.black, 18),
                  leading: Icon(Icons.delete),
                  onTap: (){
                    Navigator.of(context).pop();
                    updatePicture(pic_default_bus);
                  },
                )
              ],
            )
        );
      },
    );
  }



  showSheetActionsEdit(BuildContext context, label, value, id_field, id_bussines, type_edit, opt){
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(5.0))),
        context: context,
        isScrollControlled: true,
        isDismissible: false,
        enableDrag:false,
        builder: (context) => SingleChildScrollView(
          child: Container(
            padding:EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal:15, vertical: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: NormalText(label+':', Colors.black, 20),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: TextField(
                      autofocus: true,
                      autocorrect: false,
                      controller: txtEdit,
                      keyboardType: type_edit,
                      decoration: InputDecoration(
                          hintText: value
                      ),
                      onSubmitted: (value) {
                        //print(value);
                      },
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: Row(
                      children: [
                        FlatButton(onPressed: (){
                          if(txtEdit.text != ''){
                            Navigator.pop(context);
                            UpdateInfo(id_field, id_bussines, opt, txtEdit.text.trim());
                            txtEdit.text = '';
                          }
                        }, child: NormalText('Ok', Colors.black, 15), color: Colors.grey[300]),
                        Container(margin: EdgeInsets.symmetric(horizontal: 2.0)),
                        FlatButton(onPressed: ()=> Navigator.pop(context), child: NormalText('Cancelar', Colors.black, 15), color: Colors.grey[300])
                      ],
                    ),
                  ),
                  SizedBox(height: 5),
                ],
              ),
            ),
          )
        ));
  }

  UpdateInfo(field, id, opt, param){
    return business
        .doc(id)
        .update({field:param})
        .then((value) => {
          if(opt== 1){
            setState((){
              _name = param;
            })
          }else if(opt == 2){
            setState((){
              _desc = param;
            })
          }else if(opt == 3){
            setState((){
              _tel = param;
            })
          }else if(opt == 4){
            setState((){
              _web = param;
            })
          }else if(opt == 5){
            setState((){
              _email = param;
            })
          }
    }).catchError((error) => print("Failed to update user: $error"));
  }

  Future<Null> _pickImage() async {
    imageFile = await FilePicker.getFile(type: FileType.image);
    if (imageFile != null) {
      _cropImage();
    }
  }

  Future<Null> _cropImage() async {
    File croppedFile = await ImageCropper.cropImage(
        sourcePath: imageFile.path,
        aspectRatioPresets: [CropAspectRatioPreset.square],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Recortar imagen',
            toolbarColor: Color(0xff977EF2),
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          title: 'Recortar imagen',
        ));
    if (croppedFile != null) {
      setState(() {
        //visibleLoading = true;
        imageFile = croppedFile;
      });
      ConvertImageToBase64(croppedFile);
    }
  }

  void ConvertImageToBase64(File image)async{
    List<int> imageBytes = await image.readAsBytesSync();

    var nameImage = Uuid().v1();
    var PathImage = "/users/${widget.user_id}/bussines/$nameImage.jpg";
    final StorageReference storageReference = FirebaseStorage().ref().child(PathImage);

    final StorageUploadTask uploadTask = storageReference.putData(imageBytes);

    final StreamSubscription<StorageTaskEvent> streamSubscription = uploadTask.events.listen((event) {
      print('EVENT ${event.type}');
    });

    await uploadTask.onComplete;
    streamSubscription.cancel();

    var image_profile = (await storageReference.getDownloadURL()).toString();
    updatePicture(image_profile);
  }

  updatePicture(img){
    return business
        .doc(widget.info.id.toString())
        .update({'logo':img})
        .then((value) => {
      setState(() {
        Newpic = img;
        //visibleLoading = false;
      }),

    })
        .catchError((error) => print("Failed to update user: $error"));
  }


}


