import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';


class ViewPhotoScreen extends StatefulWidget{
  var galleryItems;
  int initialIndex;
  ViewPhotoScreen(this.galleryItems, this.initialIndex):pageController = PageController(initialPage: initialIndex);

  PageController pageController;

  @override
  ViewPhotoScreenState createState() => ViewPhotoScreenState();
}

class ViewPhotoScreenState extends State<ViewPhotoScreen>{
  int currentIndex;

  @override
  void initState() {
    currentIndex = widget.initialIndex;
    super.initState();
  }



  void onPageChanged(int index) {
    setState(() {
      currentIndex = index;
    });
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Container(
        child: PhotoViewGallery.builder(
          scrollPhysics: const BouncingScrollPhysics(),
          builder: (BuildContext context, int index) {
            return PhotoViewGalleryPageOptions(
              imageProvider: NetworkImage(widget.galleryItems[index]['photo']),
              heroAttributes: PhotoViewHeroAttributes(tag: 'Picture'),
            );
          },
          itemCount: widget.galleryItems.length,
          loadingBuilder: (context, event) => Center(
            child: Container(
              width: 20.0,
              height: 20.0,
              child: CircularProgressIndicator(
                value: event == null
                    ? 0
                    : event.cumulativeBytesLoaded / event.expectedTotalBytes,
              ),
            ),
          ),
          pageController: widget.pageController,
          onPageChanged: onPageChanged,
        )
      ),
    );
  }

}