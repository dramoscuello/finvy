import 'package:shared_preferences/shared_preferences.dart';
import 'package:whereappv2/src/screens/chatScreen.dart';
import 'package:whereappv2/src/utils/TextStyles.dart';
import 'bussinesScreen.dart';
import 'servicesScreen.dart';
import 'settingsScreen.dart';
import '../config/sqlite_info_user.dart';
import 'ProfileScreen.dart';
import 'package:whereappv2/src/config/shap_location.dart';
import 'searchScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart' as lc;
import 'package:permission_handler/permission_handler.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';



class HomeScreen extends StatefulWidget{
  @override
  HomeScreenState createState() => HomeScreenState();
}


const DEFAULT_LOC = LatLng(9.2320559, -75.8166036);

class HomeScreenState extends State<HomeScreen>{
  BitmapDescriptor icon;
  lc.Location location;
  bool locationEnabled = false;
  bool buttonLocationEnabled = false;
  LatLng currentLocation;
  LatLng firstLocation;

  GoogleMapController controller;

  var scaffoldKey = GlobalKey<ScaffoldState>();
  var db = new ManageDb();


  var sloc = new LocationSaved();

  String msjDrawer = "";

  var available = false;

  List<Map> lista;
  int id_user;
  String name_user;
  bool AcceptedTermsServices;
  bool searchingService = false;

  CollectionReference users = FirebaseFirestore.instance.collection('users_');

  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};

  onMapcreate(GoogleMapController controller) async{
    //funcion asincrona que se ejecuta cuando el mapa se ha cargado
    controller.setMapStyle(await rootBundle.loadString('resources/styles/mapRetro.json'));//carga el estilo del mapa que se encuentra en json
    setState(() {
      this.controller = controller;
    });
    await getDataUser();
    await requesPerms();
  }

  @override
  void initState() {
    getSharedLoc();
    getImage();
     initializeDefault();
  }

  Future<void> initializeDefault() async {
    FirebaseApp app = await Firebase.initializeApp();
    assert(app != null);
    print('Initialized default app $app');
  }



  Future<void> updateUserFire(lat, lon, id_doc) {
    return users
        .doc(id_doc.toString())
        .update({'location':GeoPoint(lat, lon)})
        .then((value) => print("usuario actualizado"))
        .catchError((error) => print("Failed to update user: $error"));
  }

  getDataUser()async{
    //en esta funcion requerimos los datos(disponible, idperfil, terminosAceptados y numero de servicios que presta) del usuario en sesion
    lista = await db.ListUserInfo();
    setState(() {
      available = lista[0]['available'].toString() == 'false'?false:true;
      id_user = lista[0]['id_user'];
      name_user = lista[0]['name'];
      AcceptedTermsServices = lista[0]['terms_serv'].toString() == 'false'?false:true;
    });
  }


  Future getSharedLoc()async{
    String result;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    result = prefs.getString('location') ?? '';
    if(result == ''){
      setState(() {
        currentLocation  = DEFAULT_LOC;

      });
    }else{
      setState(() {
        var coordenadas = result.split(',');
        currentLocation  = LatLng(double.parse(coordenadas[0]), double.parse(coordenadas[1]));

      });
    }
  }


  getLocation()async{
    //funcion que retorna la ubicacion actual y actualiza la ubicacion en el mapa a traves de la funcion updateLocation()
    var currentLocation = await location.getLocation();
    await updateLocation(currentLocation);
    await sloc.LocationSave(currentLocation.latitude.toString() + ',' + currentLocation.longitude.toString());
    updateUserFire(currentLocation.latitude, currentLocation.longitude, id_user);
  }

  updateLocation(currentLocation)async{
    if(currentLocation != null){
      setState(() {
        this.currentLocation = LatLng(currentLocation.latitude, currentLocation.longitude);
        this.controller.animateCamera(CameraUpdate.newCameraPosition(
            CameraPosition(target: this.currentLocation, zoom: 16)
        ));
      });
    }
  }

  locationChanged(){
    //funcion que escucha cuando la ubicacion del usuario cambie para actualizar el mapa y la ubicacion de este en la base de datos
    location.onLocationChanged.listen((lc.LocationData cloc)async{
      if (cloc != null){
        updateLocation(cloc);
        if(firstLocation == null){
          firstLocation = LatLng(cloc.latitude, cloc.longitude);
        }else{
          //TENER EN CUENTA QUE LA LONGITUD EN OTROS LUGARES NO ES NEGATIVA Y EL SIGNO MENOS, POR CONSIGUIENTE NO SE CONTARÁ EN ESE LUGAR, EL SUBSTRING CONTARÁ UN NÚMERO MAS EN ESE CASO
          if((firstLocation.longitude.toString().substring(0, 8) != cloc.longitude.toString().substring(0, 8))&&(firstLocation.latitude.toString().substring(0, 6) != cloc.latitude.toString().substring(0, 6))){
            firstLocation = LatLng(cloc.latitude, cloc.longitude);
            updateUserFire(cloc.latitude, cloc.longitude, id_user);
          }
        }
      }
    });
  }

  requesPerms()async{
    //funcion que requiere al dispositivo permisos para la localización, si los acepta se ejecuta la función "activateGps"
    Map<Permission, PermissionStatus>statuses = await [Permission.locationAlways].request();
    var status = statuses[Permission.locationAlways];
    if(status == PermissionStatus.denied){
      requesPerms();
    }else{
      await activateGps();
    }
  }

  activateGps() async{
    //Funcion que pide acceso al gps sino esta activado, cuando el gps este activo, se ejecutan las funciones
    location = lc.Location();
    bool serviceStatusResult = await location.requestService();

    if (!serviceStatusResult){
      activateGps();
    }else{
      await updatePreferencesLocation();
      await getLocation();
      await locationChanged();
    }
  }

  updatePreferencesLocation(){
    //funcion que cambia el estado a ciertos parametros en el mapa que estaban en false y no estaban activos y visibles
    //como es el boton de ir a la ubicacion y el circulo que muestra la ubicacion actual
    setState(() {
      locationEnabled = true;
      buttonLocationEnabled = true;
    });
  }


  getImage() async{
    //funcion que perzonaliza el marcador en el mapa
    //cuando se quiere usar imagenes en la aplicacion se deben importar primero en el archivo "pubspec.yaml"
    var icon  = await BitmapDescriptor.fromAssetImage(ImageConfiguration(devicePixelRatio: 1.0), 'resources/images/marker.png');
    setState(() {
      this.icon = icon;
    });
  }

  updateAvailable(bool av){
    return users
        .doc(id_user.toString())
        .update({'available':av})
        .then((value) => {
          print("estado actualizado"),
          db.UpdateAvailable(id_user, av.toString())
        })
        .catchError((error) => print("Failed to update user: $error"));
  }




  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return WillPopScope(
      child:Scaffold(
        backgroundColor: Colors.white,
        key: scaffoldKey,
          drawer: Drawer(
            child: ListView(
                padding: EdgeInsets.zero,
                children: <Widget>[
                    DrawerHeader(
                      child: Center(
                          child: NormalText(msjDrawer,Color(0xffffffff), 25)
                      ),
                    decoration: BoxDecoration(
                        color: Color(0xff977EF2)
                    ),
                  ),
                  ListTile(
                    trailing: Icon(FontAwesomeIcons.user),
                    title: NormalText("Perfil",Color(0xff000000), 18),
                    onTap: (){
                      Navigator.pop(context);
                      Navigator.push(context, MaterialPageRoute(builder: (context) => ProfileScreen(id_user)))
                          .then((value){
                        getDataUser();
                      });
                    },
                  ),
                  ListTile(
                    trailing: Icon(FontAwesomeIcons.hammer),
                    title: NormalText("Mis servicios",Color(0xff000000), 18),
                    onTap: (){
                      Navigator.pop(context);
                      //REDIRECCIONAMOS A LA VISTA DE SERVICIOS Y LE ENVIAMOS EL NUMEROS DE SERVICIOS DEL USUARIO, ID, Y SI ACEPTO LOS TERMINOS PARA PRESTAR SERVICIOS
                      Navigator.push(context, MaterialPageRoute(builder: (context) => ServiceScreen(id_user, lista[0]['nservices'], AcceptedTermsServices)))
                          .then((value){//PROMESA QUE SE EJECUTA CUANDO SE VUELVE AL HOME DESPUES DE IR A SERVICIOS Y REFRESCA LO QUE HEMOS TRAIDO DE LA DB LOCAL YA QUE
                            //SE PUDO HABER AGREGADO UN NUEVO SERVICIO
                        getDataUser();
                      });
                    },
                  ),
                  ListTile(
                    title: NormalText("Chats",Color(0xff000000), 18),
                    trailing: Icon(FontAwesomeIcons.comments),
                    onTap: (){
                      Navigator.pop(context);
                      Navigator.push(context, MaterialPageRoute(builder: (context) => chatScreen()));
                    },
                  ),
                  ListTile(
                    title: NormalText("Notificaciones",Color(0xff000000), 18),
                    trailing: Icon(FontAwesomeIcons.bell),
                    onTap: (){
                      Navigator.pop(context);
                      //Navigator.push(context, MaterialPageRoute(builder: (context) => NotificationsScreen()));
                    },
                  ),
                  ListTile(
                    title: NormalText("Ajustes",Color(0xff000000), 18),
                    trailing: Icon(FontAwesomeIcons.cogs),
                    onTap: (){
                      Navigator.pop(context);
                      Navigator.push(context, MaterialPageRoute(builder: (context) => SettingsScreen(id_user)));
                    },
                  ),
                  const Divider(
                    color: Colors.black,
                    height: 0.5,
                    thickness: 0.5,
                    indent: 0,
                    endIndent: 0,
                  ),

                    ListTile(
                      title: NormalText("Disponible",Color(0xff000000), 18),
                      trailing: Switch(value: available,
                        onChanged:(value){
                          setState(() {
                            available = value;
                          });

                          updateAvailable(available);
                        },
                        activeColor: Color(0xff977EF2),
                      ),
                    ),
                ]
            ),
          ),
          body: StreamBuilder(
              stream: users
                  .where('available', isEqualTo: true)
                  .snapshots(),
              builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
                if(!snapshot.hasData){
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }

                Map<MarkerId, Marker> _markersMap = <MarkerId, Marker>{};


                for (int i=0; i<snapshot.data.docs.length; i++){
                  MarkerId markerId = MarkerId(snapshot.data.docs[i].id.toString());
                  Marker marker = new Marker(
                    icon: this.icon==null?BitmapDescriptor.defaultMarker:this.icon,
                    markerId: markerId,
                    position: LatLng(snapshot.data.docs[i]['location'].latitude,snapshot.data.docs[i]['location'].longitude),
                  );
                  _markersMap[markerId] = marker;
                }

                return Stack(
                  children: <Widget>[
                    GoogleMap(
                      initialCameraPosition:CameraPosition(
                          target:currentLocation,
                          zoom: 16
                      ),
                      myLocationEnabled: locationEnabled,
                      myLocationButtonEnabled: buttonLocationEnabled,
                      mapType: MapType.normal,
                      rotateGesturesEnabled: false,
                      tiltGesturesEnabled: false,
                      zoomControlsEnabled:false,
                      onMapCreated: onMapcreate,
                      markers: Set<Marker>.of(_markersMap.values),
                    ),
                    Positioned(
                      left: 10,
                      top: 20,
                      child: IconButton(//icono que nos permitira mostrar el drawer menu de opciones
                        icon: Icon(FontAwesomeIcons.bars),
                        onPressed: (){
                          setMsjDrawer();
                          if(id_user != null){
                            scaffoldKey.currentState.openDrawer();
                          }
                        },
                      ),
                    ),
                  ],
                );
              }
          ),
        floatingActionButton: FloatingActionButton.extended(//boton flotante que permitira buscar servicios y mostrarlos en el mapa,
          //cuando se selecciona esta boton nos muestra la vista SearchScreen()
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => SearchScreen()));
          },
          label: NormalText("Buscar",Color(0xffffffff), 18),
          icon: Icon(FontAwesomeIcons.search, color: Colors.white,),
          backgroundColor: Color(0xff977EF2),
        ),
      ),
        onWillPop:(){
          BackHome();
        }
    );
  }

  void BackHome(){
      SystemNavigator.pop();
  }


  void setMsjDrawer(){
    var hora  = DateTime.now().hour;
    if((hora >= 18 && hora <=23) || (hora == 0)){
      setState(() {
        msjDrawer = "Buenas noches";
      });
    }else if(hora >= 12 && hora<18){
      setState(() {
        msjDrawer = "Buenas tardes";
      });
    }else if(hora >= 6 && hora<12){
      setState(() {
        msjDrawer = "Buenos dias";
      });
    }else{
      setState(() {
        msjDrawer = "Feliz madrugada";
      });
    }
  }

  void snackMsj(String msj, GlobalKey<ScaffoldState> _scaffold, Color color){
    _scaffold.currentState.showSnackBar(SnackBar(content: Text(msj),
        backgroundColor:color));
  }
}