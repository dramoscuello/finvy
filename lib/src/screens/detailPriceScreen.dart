import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:whereappv2/src/Widget/backButton.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:whereappv2/src/utils/TextStyles.dart';


class DetailPrice extends StatefulWidget {
  String lbl_title;
  String desc;
  int price;
  var idp;
  String idprofesion_perfil;
  int user_id;

  DetailPrice(this.lbl_title, this.desc, this.price, this.idp, this.idprofesion_perfil, this.user_id);
  @override
  DetailPriceState createState() => DetailPriceState();
}

class DetailPriceState extends State<DetailPrice> {

  DetailPriceState();



  TextEditingController txtDesc = TextEditingController();
  TextEditingController txtPrice = TextEditingController();
  CollectionReference price;




  GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();




  void snackMsj(String msj){//funcion que muestra mensajes snack, aquello que se muestran en la parte inferior del dispositivo
    _scaffold.currentState.showSnackBar(SnackBar(content: Text(msj),
        backgroundColor:Colors.red));
  }

  @override
  void initState() {
    super.initState();
    txtDesc.text = widget.desc;
    txtPrice.text = widget.price.toString();
    price = FirebaseFirestore.instance.collection('_users_services').doc(widget.user_id.toString()).collection('user_professions').doc(widget.idprofesion_perfil).collection('prices_profession');
  }

  saveItemPrice(){
    return price
        .add({
      'desc': txtDesc.text.trim(),
      'price':num.parse(txtPrice.text)
    }).then((value) => {
      Navigator.pop(context)
    }).catchError((error) => print("Failed to add user: $error"));
  }

  updatePrice(){
    return price
        .doc(widget.idp.toString())
        .update({'desc':txtDesc.text.trim(), 'price':num.parse(txtPrice.text)})
        .then((value) => {
      Navigator.pop(context)
    })
        .catchError((error) => print("Failed to update user: $error"));
  }

  Widget entryField1() {//campo de texto para el usuario
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText('Descripción',Colors.black, 18),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: txtDesc,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true,
                prefixIcon: Icon(FontAwesomeIcons.list)
            ),)
        ],
      ),
    );
  }

  Widget entryField2() {//campo de texto para contraseña
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText('Precio',Colors.black, 18),
          SizedBox(
            height: 10,
          ),
          TextField(
            keyboardType: TextInputType.number,
            controller: txtPrice,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true,
                prefixIcon: Icon(FontAwesomeIcons.moneyBill),
            ),)
        ],
      ),
    );
  }


  Widget _submitButton() {
    return InkWell(
        onTap: () {
          //cuando se le da clic al boton login, verifica que no esten vacios y luego concripta la contraseña y envia los datos la funcion login
          if(txtDesc.text.trim() != "" && txtPrice.text.trim() != ""){
            if(num.parse(txtPrice.text.trim()) == 0){
              snackMsj("El precio debe ser mayor a cero");
            }else{
              if(widget.idp != null){
                updatePrice();
              }else{
                saveItemPrice();
              }
            }
          }else{
            snackMsj("TODOS LOS CAMPOS SON REQUERIDOS");
          }
        },
        child:Container(
          width: MediaQuery.of(context).size.width/3,
          padding: EdgeInsets.symmetric(vertical: 5),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(45)),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.grey.shade200,
                    offset: Offset(2, 4),
                    blurRadius: 5,
                    spreadRadius: 2)
              ],
              gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [Color(0xff977EF2), Color(0xff2955D9)])),
          child: Icon(Icons.check, color: Colors.white, size: 40),
        )
    );
  }





  Widget _title() {
    return BoldText(widget.lbl_title, 30, Color(0xff000000));
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        key: _scaffold,
        body: SingleChildScrollView(
            child: Container(
              height: MediaQuery.of(context).size.height,
              child: Stack(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          flex: 3,
                          child: SizedBox(),
                        ),
                        _title(),
                        SizedBox(
                          height: 50,
                        ),
                        entryField1(),
                        entryField2(),
                        SizedBox(
                          height: 20,
                        ),
                        _submitButton(),
                        //_divider(),
                        Expanded(
                          flex: 2,
                          child: SizedBox(),
                        ),
                      ],
                    ),
                  ),
                  Positioned(top: 40, left: 0, child: backButton())
                ],
              ),
            )
        )
    );
  }
}
