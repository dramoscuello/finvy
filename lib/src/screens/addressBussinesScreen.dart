import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:whereappv2/src/Widget/widget404.dart';
import 'package:whereappv2/src/config/ApiKey.dart';
import 'package:whereappv2/src/screens/horariosScreen.dart';
import 'package:whereappv2/src/utils/TextStyles.dart';



class addressBussines extends StatefulWidget{
  String user_id;
  String bussines_id;
  addressBussines(this.user_id, this.bussines_id);

  @override
  addressBussinesState createState() => addressBussinesState();
}

class addressBussinesState extends State<addressBussines>{

  CollectionReference business = FirebaseFirestore.instance.collection('business');


  @override
  void initState() {
    super.initState();
    initializeDefault();
  }

  Future<void> initializeDefault() async {
    FirebaseApp app = await Firebase.initializeApp();
    assert(app != null);
    print('Initialized default app $app');
  }

  Widget widget_address(snapshot){
    return Stack(
      children: <Widget>[
        Container(
          margin: EdgeInsets.fromLTRB(0,10.0,0,0),
          child: ListView.separated(
              padding: EdgeInsets.fromLTRB(10.0,10.0,10.0,0),
              itemCount: snapshot.length,
              itemBuilder:(context, index){
                return Container(
                    margin: EdgeInsets.fromLTRB(0, 2, 0, 2),
                    padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.0),
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 10.0,
                          color: Colors.grey[200],
                        )
                      ],
                    ),
                    child:ListTile(
                        title: NormalText(snapshot[index]['address'],Color(0xff000000), 18),
                        trailing: Icon(Icons.location_on),
                        onTap: (){
                          showSheetActionsAddress(context, snapshot[index].id, snapshot[index]['address']);
                        }
                    )
                );
              },
              separatorBuilder: (context, index){
                return Divider();
              }
          ),
        )
      ],
    );
  }


  pick_loc([id_loc='']) async{
    LocationResult result = await showLocationPicker(
      context, API_KEY,
      initialCenter: LatLng(3.4299548, -76.5434253),
      mapStylePath: 'resources/styles/mapRetro.json',
      myLocationButtonEnabled: true,
      hintText: 'Buscar ubicación',
      desiredAccuracy: LocationAccuracy.best,
    );
    if(result.latLng.latitude.toString() != null && result.latLng.longitude.toString() != null && result.address.toString()!=null){
      if(id_loc == ''){
        showDialogAddAddress(result.address.toString(), result.latLng.latitude.toString(), result.latLng.longitude.toString());
      }else{
        showDialogAddAddress(result.address.toString(), result.latLng.latitude.toString(), result.latLng.longitude.toString(), id_loc);
      }
    }
  }

  showSheetActionsAddress(BuildContext context, id_loc, address){
    showModalBottomSheet<void>(
      context: context,
      builder: (context) {
        return Container(
            height: 180,
            color: Colors.white,
            child: ListView(
              itemExtent: 60,
              children: <Widget>[
                ListTile(
                  title: NormalText('Horarios',Colors.black, 18),
                  leading: Icon(Icons.calendar_today),
                  onTap: (){
                    Navigator.of(context).pop();
                    Navigator.push(context, MaterialPageRoute(builder: (context) => horariosBussines(widget.user_id.toString(), widget.bussines_id.toString(), id_loc)));
                  },
                ),
                ListTile(
                  title: NormalText('Editar',Colors.black, 18),
                  leading: Icon(Icons.edit),
                  onTap: (){
                    Navigator.of(context).pop();
                    pick_loc(id_loc);
                  },
                ),
                ListTile(
                  title: NormalText('Eliminar',Colors.black, 18),
                  leading: Icon(Icons.delete),
                  onTap: (){
                    Navigator.of(context).pop();
                    showDialogDelAddress(id_loc, address);
                  },
                )
              ],
            )
        );
      },
    );
  }

  Future<void> showDialogAddAddress(address, lat, lon, [id_loc='']) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25.0))
          ),
          title: NormalText('¡Confirmar!',Colors.black, 22),

          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[

                NormalText(address,Colors.black, 18),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: NormalText('Cancelar',Colors.lightBlue, 18),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: NormalText('OK',Colors.lightBlue, 18),
              onPressed: () {
                Navigator.of(context).pop();
                if(id_loc == ''){
                  addNewAddress(address, lat, lon);
                }else{
                  updateAddress(address, lat, lon, id_loc);
                }
              },
            )
          ],
        );
      },
    );
  }

  Future<void> showDialogDelAddress(id_loc, address) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25.0))
          ),
          title: NormalText('¿Eliminar?',Colors.black, 22),

          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                NormalText(address,Colors.black, 18),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: NormalText('Cancelar',Colors.lightBlue, 18),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: NormalText('OK',Colors.lightBlue, 18),
              onPressed: () {
                Navigator.of(context).pop();
                delAddress(id_loc);
              },
            )
          ],
        );
      },
    );
  }

  addNewAddress(address, lat, lon){
    return business
        .doc(widget.bussines_id.toString())
        .collection('locations_bussines')
        .add({'id_bussines_related':widget.bussines_id.toString(),
      'address':address,
      'location': GeoPoint(double.parse(lat), double.parse(lon)),
      'promoted':false,
      'status':true,
      'horarios':[],
      'promos':[]
    })
        .then((value) => {
      print("Exito")
    }).catchError((error) => print("Failed to update user: $error"));
  }

  updateAddress(address, lat, lon, id_loc){
    return business
        .doc(widget.bussines_id.toString())
        .collection('locations_bussines')
        .doc(id_loc.toString())
        .update({
      'address':address,
      'location': GeoPoint(double.parse(lat), double.parse(lon))
    })
        .then((value) => {
      print("Exito")
    }).catchError((error) => print("Failed to update user: $error"));
  }

  delAddress(id_address){
    return business
        .doc(widget.bussines_id.toString())
        .collection('locations_bussines')
        .doc(id_address)
        .delete()
        .then((value) => {
      print("Exito")
    }).catchError((error) => print("Failed to update user: $error"));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: NormalText("Direcciones", Colors.black, 19),
        centerTitle: true,
        leading: IconButton(icon: Icon(Icons.close), color:Colors.black ,onPressed: (){
          Navigator.pop(context);
        },),
        backgroundColor: Colors.white,
        actions: [
          IconButton(icon: Icon(Icons.add), color:Colors.black ,onPressed: (){
            pick_loc();
          },)
        ],
      ),
      body: StreamBuilder(
          stream: business.doc(widget.bussines_id.toString()).collection('locations_bussines').snapshots(),
          builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
            if(!snapshot.hasData){
              return Center(
                child: CircularProgressIndicator(),
              );
            }else{
              if(snapshot.data.docs.length > 0){
                return widget_address(snapshot.data.docs);
              }else{
                return Widget404();
              }
            }
          }
      )
    );
  }

}