import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:whereappv2/src/screens/statsServicesScreen.dart';
import 'package:whereappv2/src/utils/TextStyles.dart';
import 'photos_servicesScreen.dart';
import 'prices_servicesScreen.dart';


class DetailService extends StatefulWidget{
  String profUserId;
  String title;
  int user_id;
  DetailService(this.profUserId, this.title, this.user_id);

  @override
  DetailServiceState createState() => DetailServiceState();
}

class DetailServiceState extends State<DetailService> with TickerProviderStateMixin{

  DetailServiceState();
  TabController _tabController;
  int page = 0;



  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 2);
  }


  @override
  void dispose(){
    super.dispose();
    _tabController.dispose();
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: NormalText(widget.title,Colors.white, 20),
            centerTitle: true,
            backgroundColor: Color(0xff977EF2),

            bottom: TabBar(
              controller: _tabController,
              tabs: <Widget>[
              Tab(
                icon: Icon(FontAwesomeIcons.chartBar),
                text: 'Estadisticas',
              ),
              Tab(
                icon: Icon(Icons.monetization_on),
                text: 'Precios',
              ),
            ],
              onTap: (index){
                setState(() {
                  page = index;
                });
              },
            ),
          ),
          body:TabBarView(
            controller: _tabController,
            children: <Widget>[
              statsServices(),
              PricesServices(widget.profUserId, widget.user_id)
            ],
          ),
    );
  }
}