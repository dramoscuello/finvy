
import 'package:flutter/material.dart';
import 'package:whereappv2/src/Widget/ProgresDialog.dart';
import 'package:whereappv2/src/utils/TextStyles.dart';
import 'package:whereappv2/src/welcomePage.dart';

import '../config/sqlite_info_user.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';



class SettingsScreen extends StatefulWidget {
  int idp;
  SettingsScreen(this.idp);

  @override
  _SettingState createState() => _SettingState(this.idp);
}

class _SettingState extends State<SettingsScreen> {
  int idp;
  _SettingState(this.idp);

  var db = new ManageDb();

  var showD = new showProgressDialog();
  final GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();


  @override
  void initState() {
    initializeDefault();
  }

  Future<void> initializeDefault() async {
    FirebaseApp app = await Firebase.initializeApp();
    assert(app != null);
    print('Initialized default app $app');
  }



  Future<void> _showMyDialog(int idp) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25.0))
          ),
          title: NormalText("¿Confirmar?",Color(0xff000000), 22),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                NormalText('¡Se cerrará la sesión!',Color(0xff000000), 18),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: NormalText('Cancelar',Colors.lightBlue, 18),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: NormalText('OK',Colors.lightBlue, 18),
              onPressed: () {
                Navigator.of(context).pop();
                cerrarSesion(idp);
              },
            )
          ],
        );
      },
    );
  }

  cerrarSesion(int idp){
    CollectionReference users = FirebaseFirestore.instance.collection('users');
    return users
        .doc(idp.toString())
        .update({'available':false})
        .then((value) => {
          db.DeleteDb(),
          showD.showDialog(context, "Espere..."),
          Future.delayed(Duration(milliseconds: 500)).then((value) {
            showD.hideShowDialog();
            Navigator.push(context, MaterialPageRoute(builder: (context) => WelcomePage()));
          })
        })
        .catchError((error) => print("Failed to update user: $error"));
  }

  Widget ListInfo(){
    return ListView(
      children: <Widget>[
        ListTile(
          title: NormalText('Lenguaje',Colors.black, 18),


          trailing: Icon(Icons.flag),
        ),

        ListTile(
          title: NormalText('Info',Colors.black, 18),


          trailing: Icon(Icons.help),
        ),
        ListTile(
          title: NormalText('Cerrar sesión',Colors.black, 18),
          trailing: Icon(Icons.exit_to_app),
          onTap: ()async{
            await _showMyDialog(idp);
          },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffold,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor:Color(0xff977EF2),
        title: NormalText('Ajustes',Colors.white, 20),
      ),
      body: Container(
          padding: EdgeInsets.fromLTRB(10.0,10.0,10.0,0),
          child: Stack(
            children: <Widget>[
              ListInfo()
            ],
          )
      ),
    );
  }
}
