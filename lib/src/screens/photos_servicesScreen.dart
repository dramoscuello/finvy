import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:whereappv2/src/Widget/loadingWidget.dart';
import 'package:whereappv2/src/Widget/widget404.dart';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:whereappv2/src/utils/TextStyles.dart';
import 'viewPhotoScreen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:uuid/uuid.dart';



class PhotoServices extends StatefulWidget{
  String id;
  int user_id;
  PhotoServices(this.id, this.user_id);


  @override
  PhotoServicesState createState() => PhotoServicesState();
}

class PhotoServicesState extends State<PhotoServices> {

  PhotoServicesState();

  CollectionReference photo;

  File file;
  bool loading = false;


  List<Map> imagenes;

  @override
  void initState() {
    photo = FirebaseFirestore.instance.collection('users').doc(
        widget.user_id.toString()).collection('user_profession')
        .doc(widget.id)
        .collection('photo_profession');
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white,
      body: loading?LoadingWidget(100, 100, 8):StreamBuilder(
          stream: photo.snapshots(),
          builder: (BuildContext context,
              AsyncSnapshot<QuerySnapshot> snapshot) {
            if (!snapshot.hasData) {
              return Center(
                child: LoadingWidget(100, 100, 8),
              );
            } else {
              if (snapshot.data.docs.length > 0) {
                return _buildGrid(snapshot.data.docs);
              } else {
                return Widget404();
              }
            }
          }
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          addImage();
        },
        label: Text("Añadir"),
        icon: Icon(Icons.add_circle),
      ),
    );
  }


  Widget _buildGrid(data) {
    return GridView.count(
        crossAxisCount: data.length > 1 ? 2 : 1,
        children: _buildGridTileList(data.length, data));
  }


  List<Card> _buildGridTileList(int count, snapshot) {
    return List.generate(
        count, (i) =>
        Card(
            elevation: 10.0,
            child: InkWell(
              child: Image.network(snapshot[i]['photo']),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => ViewPhotoScreen(snapshot, i)));
              },
              onLongPress: () {
                showSheetActions(context, snapshot[i].id);
              },
            )

        ));
  }


  showSheetActions(BuildContext context, String id) {
    showModalBottomSheet<void>(
      context: context,
      builder: (context) {
        return Container(
            height: 180,
            color: Colors.white,
            child: ListView(
              itemExtent: 60,
              children: <Widget>[
                ListTile(
                  title: NormalText('Editar', Colors.black, 18),
                  leading: Icon(Icons.edit),
                  onTap: () {
                    print(id);
                    Navigator.of(context).pop();
                  },
                ),
                ListTile(
                  title: NormalText('Eliminar', Colors.black, 18),
                  leading: Icon(Icons.delete),
                  onTap: () async {
                    await Navigator.of(context).pop();
                    await showDialogDelete(id);
                  },
                ),
                ListTile(
                  title: NormalText('Info', Colors.black, 18),
                  leading: Icon(Icons.info),
                  onTap: () {
                    print(id);
                    Navigator.of(context).pop();
                  },
                )
              ],
            )
        );
      },
    );
  }


  delete_photo(String id) async {
    return photo
        .doc(id)
        .delete()
        .then((value) => print("User deleted"))
        .catchError((error) => print("Failed to delete user: $error"));
  }


  addImage() async {
    try {
      file = await FilePicker.getFile(type: FileType.image);
      if (file != null) {
        showCustomDialogWithImage(context, file);
      }
    } catch (e) {
      print(e);
    }
  }


  Future<void> showDialogDelete(String id) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25.0))
          ),
          title: NormalText('¿Confirmar?', Colors.black, 22),

          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[

                NormalText('¡El elemento se eliminará!', Colors.black, 18),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: NormalText('Cancelar', Colors.lightBlue, 18),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: NormalText('OK', Colors.lightBlue, 18),
              onPressed: () {
                Navigator.of(context).pop();
                delete_photo(id);
              },
            )
          ],
        );
      },
    );
  }


  void showCustomDialogWithImage(BuildContext context, File image) {
    Dialog dialogWithImage = Dialog(
      child: Container(
        height: 300.0,
        width: 300.0,
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(12),
              alignment: Alignment.center,
              decoration: BoxDecoration(color: Colors.grey[300]),
              child: NormalText('Previsualización', Colors.black, 18),
            ),
            Container(
              height: 200,
              width: 300,
              child: Image.file(image,
                fit: BoxFit.contain,
              ),

            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                RaisedButton(
                  color: Color(0xff977EF2),
                  onPressed: () {
                    previewImage(image);
                    Navigator.of(context).pop();
                  },
                  child: NormalText('Añadir', Colors.white, 18),
                ),
                SizedBox(
                  width: 20,
                ),
                RaisedButton(
                  color: Colors.red,
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: NormalText('Cancelar', Colors.white, 18),
                )
              ],
            ),
          ],
        ),
      ),
    );
    showDialog(
        context: context, builder: (BuildContext context) => dialogWithImage);
  }

  previewImage(File preview) async {
    var nameImage = Uuid().v1();
    var PathImage = "/professions/users/${widget.user_id}/$nameImage.jpg";
    final StorageReference storageReference = FirebaseStorage().ref().child(PathImage);

    final StorageUploadTask uploadTask = storageReference.putFile(preview);

    final StreamSubscription<StorageTaskEvent> streamSubscription = uploadTask.events.listen((event) {
      print('EVENT ${event.type}');
    });

    setState(() {
      loading = true;
    });

    await uploadTask.onComplete;

    streamSubscription.cancel();

    setState(() {
      loading = false;
    });

    var imagenFinal = (await storageReference.getDownloadURL()).toString();
    await uploadPic(imagenFinal);
  }

  uploadPic(image){
    return photo
        .add({
      'photo': image,
    }).then((value) => {
    }).catchError((error) => print("Failed to add user: $error"));
  }

}