import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:uuid/uuid.dart';
import 'package:whereappv2/src/Widget/loadingWidget.dart';
import 'package:whereappv2/src/config/ApiKey.dart';
import 'package:whereappv2/src/utils/TextStyles.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:file_picker/file_picker.dart';




class addBussiness extends StatefulWidget{
  int id_user;
  addBussiness(this.id_user);

  @override
  _StateAddBussiness createState() => _StateAddBussiness();
}



class _StateAddBussiness extends State<addBussiness>{

  _StateAddBussiness();

  GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();

  String lng;
  String lat;
  int pos = 0;

  List<int> imageBytes;

  TextEditingController txtLoc = TextEditingController();
  TextEditingController txtNit = TextEditingController();
  TextEditingController txtName = TextEditingController();
  TextEditingController txtDesc = TextEditingController();
  TextEditingController txtTel = TextEditingController();
  TextEditingController txtEmail = TextEditingController();
  TextEditingController txtWeb = TextEditingController();
  TextEditingController txtLogo = TextEditingController();

  File imageFile;


  CollectionReference business = FirebaseFirestore.instance.collection('business');
  bool loading = false;


  Widget _title() {
    return BoldText("Mi negocio", 30, Color(0xff000000));
  }

  Widget entryField1() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText("NIT*:",Colors.black, 19),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: txtNit,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true,
                prefixIcon: Icon(FontAwesomeIcons.list)
            ),)
        ],
      ),
    );
  }
  Widget entryField2() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText("Nombre*:",Colors.black, 19),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: txtName,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true,
                prefixIcon: Icon(FontAwesomeIcons.list)
            ),)
        ],
      ),
    );
  }

  Widget entryField3() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText("Descripción*:",Colors.black, 19),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: txtDesc,
            keyboardType: TextInputType.multiline,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true,
                prefixIcon: Icon(FontAwesomeIcons.list)
            ),)
        ],
      ),
    );
  }

  Widget entryField4() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText("Telefono*:",Colors.black, 19),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: txtTel,
            keyboardType: TextInputType.phone,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true,
                prefixIcon: Icon(FontAwesomeIcons.list)
            ),)
        ],
      ),
    );
  }

  Widget entryField5() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText("E-mail: ",Colors.black, 19),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: txtEmail,
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true,
                prefixIcon: Icon(FontAwesomeIcons.list)
            ),)
        ],
      ),
    );
  }

  Widget entryField6() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText("Sitio web: ",Colors.black, 19),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: txtWeb,
            keyboardType: TextInputType.url,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true,
                prefixIcon: Icon(FontAwesomeIcons.list)
            ),)
        ],
      ),
    );
  }




  Widget entryField8() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText("Dirección*:",Colors.black, 19),
          SizedBox(
            height: 10,
          ),
          TextField(
            readOnly: true,
            controller: txtLoc,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true
            ),)
        ],
      ),
    );
  }

  Widget entryField9() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText("Logo*:",Colors.black, 19),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: txtLogo,
            readOnly: true,
            decoration: InputDecoration(
                border: InputBorder.none,
                fillColor: Color(0xfff3f3f4),
                filled: true
            ),)
        ],
      ),
    );
  }

  Widget _backButton() {
    return InkWell(
        onTap: () {
          if(pos>0){
            setState(() {
              pos--;
            });
          }
        },
        child:Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(vertical: 5),
          margin: EdgeInsets.symmetric(horizontal: 5),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(45)),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.grey.shade200,
                    offset: Offset(2, 4),
                    blurRadius: 5,
                    spreadRadius: 2)
              ],
              gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [Color(0xff977EF2), Color(0xff2955D9)])),
          child: Icon(Icons.arrow_back, color: Colors.white, size: 40),
        )
    );
  }

  Widget _nextButton() {
    return InkWell(
        onTap: () {
          switch(pos){
            case 0:{
              if(txtNit.text != '' && txtName.text != '' && txtDesc.text != ''){
                setState(() {
                  pos++;
                });
              }else{
                snackMsj('Todos los campos son requeridos');
              }
            }
            break;
            case 1:{
                if(txtTel.text != ''){
                  setState(() {
                    pos++;
                  });
                }else{
                  snackMsj('Algunos campos son requeridos');
                }
            }
            break;
            case 2:{
              if(txtLoc.text != '' && txtLogo.text != ''){
                loadImage();
              }else{
                snackMsj('Todos los campos son requeridos');
              }
            }
            break;
          }
        },
        child:Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(vertical: 5),
          margin: EdgeInsets.symmetric(horizontal: 5),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(45)),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.grey.shade200,
                    offset: Offset(2, 4),
                    blurRadius: 5,
                    spreadRadius: 2)
              ],
              gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [Color(0xff977EF2), Color(0xff2955D9)])),
          child: pos<2?Icon(Icons.arrow_forward, color: Colors.white, size: 40):Icon(Icons.check, color: Colors.white, size: 40),
        )
    );
  }

  Widget pick_loc() {
    return InkWell(
        onTap: () async{
          LocationResult result = await showLocationPicker(
            context, API_KEY,
            initialCenter: LatLng(3.4299548, -76.5434253),
            mapStylePath: 'resources/styles/mapRetro.json',
            myLocationButtonEnabled: true,
            hintText: 'Buscar ubicación',
            desiredAccuracy: LocationAccuracy.best,
          );
          txtLoc.text = result.address.toString();
          lat = result.latLng.latitude.toString();
          lng = result.latLng.longitude.toString();
        },
        child:Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(vertical: 5),
          margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(45)),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.grey.shade200,
                    offset: Offset(2, 4),
                    blurRadius: 5,
                    spreadRadius: 2)
              ],
              gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [Color(0xff977EF2), Color(0xff2955D9)])),
          child:Icon(Icons.add_location, color: Colors.white, size: 40),
        )
    );
  }

  Widget btn_add_logo() {
    return InkWell(
        onTap: () async{
          await _pickImage();
        },
        child:Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(vertical: 5),
          margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(45)),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.grey.shade200,
                    offset: Offset(2, 4),
                    blurRadius: 5,
                    spreadRadius: 2)
              ],
              gradient: LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [Color(0xff977EF2), Color(0xff2955D9)])),
          child:Icon(Icons.add_photo_alternate, color: Colors.white, size: 40),
        )
    );
  }



  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
          backgroundColor: Colors.white,
          key: _scaffold,
          body: loading?LoadingWidget(100, 100, 8):SingleChildScrollView(
              child: Container(
                height: MediaQuery.of(context).size.height,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            flex: 3,
                            child: SizedBox(),
                          ),
                          _title(),
                          SizedBox(
                            height: 50,
                          ),
                          Visibility(
                              visible: pos==0?true:false,
                              child: Wrap(
                                children: [
                                  entryField1(),
                                  entryField2(),
                                  entryField3()
                                ],
                              )
                          ),
                          Visibility(
                              visible: pos==1?true:false,
                              child: Wrap(
                                children: [
                                  entryField4(),
                                  entryField5(),
                                  entryField6()
                                ],
                              )
                          ),
                          Visibility(
                              visible: pos==2?true:false,
                              child: Wrap(
                                children: [
                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment:CrossAxisAlignment.end,
                                      children: <Widget>[
                                        Expanded(
                                          flex: 1,
                                          child: entryField9(),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: btn_add_logo(),
                                        )
                                      ]
                                  ),
                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment:CrossAxisAlignment.end,
                                      children: <Widget>[
                                        Expanded(
                                          flex: 1,
                                          child: entryField8(),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: pick_loc(),
                                        )
                                      ]
                                  )
                                ],
                              )
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Expanded(
                                    flex: 1,
                                    child: Visibility(
                                      visible: pos>0?true:false,
                                      child: _backButton(),
                                    )
                                ),
                                Expanded(
                                  flex: 1,
                                  child: _nextButton(),
                                )
                              ]
                          ),

                          //_divider(),
                          Expanded(
                            flex: 2,
                            child: SizedBox(),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )
          )
    );
  }

  void snackMsj(String msj) {
    _scaffold.currentState.showSnackBar(SnackBar(content: NormalText(msj,Colors.white, 18),
        backgroundColor: Colors.red));
  }

  Future<Null> _pickImage() async {
    imageFile = await FilePicker.getFile(type: FileType.image);
    if (imageFile != null) {
      _cropImage();
    }
  }

  Future<Null> _cropImage() async {
    File croppedFile = await ImageCropper.cropImage(
        sourcePath: imageFile.path,
        aspectRatioPresets: [CropAspectRatioPreset.square],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Recortar imagen',
            toolbarColor: Color(0xff977EF2),
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          title: 'Recortar imagen',
        ));
    if (croppedFile != null) {
      setState(() {
        txtLogo.text = imageFile.path;
        imageFile = croppedFile;
      });
      ConvertImageToBase64(croppedFile);
    }
  }

  void ConvertImageToBase64(File image)async{
    imageBytes = image.readAsBytesSync();
  }

  loadImage()async{
    if(imageBytes != null){
      var nameImage = Uuid().v1();
      var PathImage = "/users/${widget.id_user}/bussines/$nameImage.jpg";
      final StorageReference storageReference = FirebaseStorage().ref().child(PathImage);
      final StorageUploadTask uploadTask = storageReference.putData(imageBytes);

      final StreamSubscription<StorageTaskEvent> streamSubscription = uploadTask.events.listen((event) {
        print('EVENT ${event.type}');
      });

      setState(() {
        loading = true;
      });

      await uploadTask.onComplete;
      streamSubscription.cancel();
      var image = (await storageReference.getDownloadURL()).toString();
      register_bussines(image);
    }
  }

  register_bussines(image){
    return business
        .doc(txtNit.text.trim())
        .set({
      'name': txtName.text.trim(),
      'descripcion': txtDesc.text.trim(),
      'phone': txtTel.text.toString(),
      'email':txtEmail.text.trim(),
      'website': txtWeb.text.trim(),
      'estado':true,
      'logo':image,
      'fk_user_id': widget.id_user
    }).then((value) => {
      addLocationMain()
    }).catchError((error) => print("Failed to add user: $error"));
  }

  addLocationMain(){
    return business
        .doc(txtNit.text.trim())
        .collection('locations_bussines')
        .add({
          //'doc_ref_business':business.doc(txtNit.text.trim()),
          'address': txtLoc.text.trim(),
          'location':GeoPoint(double.parse(lat), double.parse(lng)) ,
          'status':true,
          'promoted':false,
          'horarios':[],
          'promos':[]
        }).then((value) => {
          setState(() {
            loading = false;
          }),
          Navigator.pop(context)
    }).catchError((error) => print("Failed to add user: $error"));
  }
}