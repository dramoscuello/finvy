import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:whereappv2/src/Widget/widget404.dart';
import 'package:whereappv2/src/screens/addProductsScreen.dart';
import 'package:whereappv2/src/screens/promocionesBussinesScreen.dart';

import 'package:whereappv2/src/utils/TextStyles.dart';



class productsBussines extends StatefulWidget{
  String user_id;
  String bussines_id;
  productsBussines(this.user_id, this.bussines_id);

  @override
  productsBussinesState createState() => productsBussinesState();
}

class productsBussinesState extends State<productsBussines>{
  CollectionReference business = FirebaseFirestore.instance.collection('business');



  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: NormalText("Productos", Colors.black, 19),
          centerTitle: true,
          leading: IconButton(icon: Icon(Icons.close), color:Colors.black ,onPressed: (){
            Navigator.pop(context);
          },),
          backgroundColor: Colors.white,
          actions: [
            IconButton(icon: Icon(Icons.search), color:Colors.black ,onPressed: (){
              //Navigator.push(context, MaterialPageRoute(builder: (context) => addProducts(widget.user_id, widget.bussines_id)));
            },),
            IconButton(icon: Icon(Icons.add), color:Colors.black ,onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => addProducts(widget.user_id, widget.bussines_id)));
            },)
          ],
        ),
        body: StreamBuilder(
            stream: business.doc(widget.bussines_id.toString()).collection('products_bussines').snapshots(),
            builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
              if(!snapshot.hasData){
                return Center(
                  child: CircularProgressIndicator(),
                );
              }else{
                if(snapshot.data.docs.length > 0){
                  return widget_products(snapshot.data.docs);
                }else{
                  return Widget404();
                }
              }
            }
        )
    );
  }


  Widget widget_products(snapshot){
    return Stack(
      children: <Widget>[
        Container(
          margin: EdgeInsets.fromLTRB(0,10.0,0,0),
          child: ListView.separated(
              padding: EdgeInsets.fromLTRB(10.0,10.0,10.0,0),
              itemCount: snapshot.length,
              itemBuilder:(context, index){
                return Container(
                    margin: EdgeInsets.fromLTRB(0, 2, 0, 2),
                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.0),
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 10.0,
                          color: Colors.grey[200],
                        )
                      ],
                    ),
                    child:ListTile(
                        title: NormalText(snapshot[index]['product'],Color(0xff000000), 18),
                        leading: Image.network(snapshot[index]['img'], width: 50, height: 50, fit: BoxFit.contain),
                        subtitle: NormalText("\$"+snapshot[index]['price'],Colors.grey, 16),
                        onTap: (){
                          showSheetActionsProducts(context, snapshot[index].id, snapshot[index]['price']);
                        }
                    )
                );
              },
              separatorBuilder: (context, index){
                return Divider();
              }
          ),
        )
      ],
    );
  }

  showSheetActionsProducts(BuildContext context, id, price){
    showModalBottomSheet<void>(
      context: context,
      builder: (context) {
        return Container(
            height: 180,
            color: Colors.white,
            child: ListView(
              itemExtent: 60,
              children: <Widget>[
                ListTile(
                  title: NormalText('Promo',Colors.black, 18),
                  leading: Icon(FontAwesomeIcons.tags),
                  onTap: (){
                    Navigator.of(context).pop();
                    Navigator.push(context, MaterialPageRoute(builder: (context) => promosBussines(widget.user_id, widget.bussines_id, id, price)));
                  },
                ),

                ListTile(
                  title: NormalText('Editar',Colors.black, 18),
                  leading: Icon(FontAwesomeIcons.edit),
                  onTap: (){
                    Navigator.of(context).pop();
                    //pick_loc(id_loc);
                  },
                ),
                ListTile(
                  title: NormalText('Eliminar',Colors.black, 18),
                  leading: Icon(FontAwesomeIcons.trash),
                  onTap: (){
                    Navigator.of(context).pop();
                    showDialogDelete(id);
                  },
                )
              ],
            )
        );
      },
    );
  }

  Future<void> showDialogDelete(String id) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25.0))
          ),
          title: NormalText('¿Confirmar?',Colors.black, 22),

          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[

                NormalText('¡El elemento se eliminará!',Colors.black, 18),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: NormalText('Cancelar',Colors.lightBlue, 18),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: NormalText('OK',Colors.lightBlue, 18),
              onPressed: () {
                Navigator.of(context).pop();
                delete_product(id);
              },
            )
          ],
        );
      },
    );
  }

  delete_product(String id){
    return business
        .doc(widget.bussines_id)
        .collection('products_bussines')
        .doc(id)
        .delete()
        .then((value) => print("Product deleted"))
        .catchError((error) => print("Failed to delete Product: $error"));
  }



}