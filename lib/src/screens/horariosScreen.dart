import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:whereappv2/src/utils/TextStyles.dart';



class horariosBussines extends StatefulWidget{

  String user_id;
  String bussines_id;
  String location_id;
  horariosBussines(this.user_id, this.bussines_id, this.location_id);

  @override
  horariosBussinesState createState() => horariosBussinesState();
}

class horariosBussinesState extends State<horariosBussines>{
  CollectionReference business = FirebaseFirestore.instance.collection('business');
  final GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();
  var _toggle_array = [false, false, false, false, false, false, false];
  var _days = ['Domingo','Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado'];
  List<dynamic> _list_horarios = List<dynamic>();
  var _array_choice_days = [];


  @override
  void initState() {
    super.initState();
    initializeDefault();
    getHorario();
  }

  Future<void> initializeDefault() async {
    FirebaseApp app = await Firebase.initializeApp();
    assert(app != null);
    print('Initialized default app $app');
  }

  Future<Null> second_hour(BuildContext context, hora_inicial, id) async {
    TimeOfDay _selectedTime = TimeOfDay(hour: 14, minute: 00);
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: _selectedTime,
      confirmText: 'Hora de cierre',
      cancelText: 'Cancelar',
    );
    if (picked != null){
      setState(() {
        if(_list_horarios.any((element) => element['day'] == id)){
          _list_horarios.removeWhere((element) => element['day'] == id);
        }
        _list_horarios.add({'day':id, 'open':hora_inicial, 'close': picked.hour.toString() + ':' + picked.minute.toString()});
      });

    }
  }

  Future<Null> first_hour(BuildContext context, id) async {
    TimeOfDay _selectedTime = TimeOfDay(hour: 06, minute: 00);
    final TimeOfDay picked = await showTimePicker(
        context: context,
        initialTime: _selectedTime,
        confirmText: 'Hora de inicio',
        cancelText: 'Cancelar'
    );
    if (picked != null)
      second_hour(context, picked.hour.toString() + ':' + picked.minute.toString(), id);
  }

  void snackMsj(String msj, Color color){//funcion que muestra mensajes snack, aquello que se muestran en la parte inferior del dispositivo
    _scaffold.currentState.showSnackBar(SnackBar(content: NormalText(msj, Colors.white, 16),
        backgroundColor:color));
  }

  setHorario(){
    return business
        .doc(widget.bussines_id.toString())
        .collection('locations_bussines')
        .doc(widget.location_id.toString())
        .update({
      'horarios':_list_horarios,
    })
        .then((value) => {
        snackMsj('Horarios actualizados', Colors.green)
    }).catchError((error) => print("Failed to update user: $error"));
  }

  getHorario(){
    FirebaseFirestore.instance
        .collection('business')
        .doc(widget.bussines_id.toString())
        .collection('locations_bussines')
        .doc(widget.location_id.toString())
        .get()
        .then((DocumentSnapshot documentSnapshot) => {
          if(documentSnapshot.data()['horarios'].length > 0){
            setState((){
              _list_horarios = documentSnapshot.data().cast()['horarios'];
            }),
            for(int i=0; i<documentSnapshot.data()['horarios'].length; i++){
              setState((){
                _toggle_array[documentSnapshot.data()['horarios'][i]['day']] = true;
                _array_choice_days.add(documentSnapshot.data()['horarios'][i]['day']);
              })
            }
          }
    });
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffold,
        appBar: AppBar(
          title: NormalText("Horarios", Colors.black, 19),
          centerTitle: true,
          leading: IconButton(icon: Icon(Icons.close), color:Colors.black ,onPressed: (){
            Navigator.pop(context);
          },),
          backgroundColor: Colors.white,
          actions: [
            IconButton(icon: Icon(Icons.check), color:Colors.black ,onPressed: (){
              if(_array_choice_days.length == _list_horarios.length){
                setHorario();
              }else{
                snackMsj('Falta asignar horas a algun(os) dia(s)', Colors.red);
              }
            },)
          ],
        ),
        body:Container(
            margin: EdgeInsets.fromLTRB(0,15.0,0,0),
            padding: EdgeInsets.fromLTRB(1,0,1,0),
            child: Column(
              children: [
                Container(
                  child: ToggleButtons(
                    children: [
                      BoldText.veryBold('D', 16, Colors.black, true),
                      BoldText.veryBold('L', 16, Colors.black, true),
                      BoldText.veryBold('M', 16, Colors.black, true),
                      BoldText.veryBold('M', 16, Colors.black, true),
                      BoldText.veryBold('J', 16, Colors.black, true),
                      BoldText.veryBold('V', 16, Colors.black, true),
                      BoldText.veryBold('S', 16, Colors.black, true),
                    ],
                    isSelected: _toggle_array,
                    onPressed: (int index){
                      setState(() {
                        _toggle_array[index] = !_toggle_array[index];
                      });

                      if(_toggle_array[index]){
                        setState((){
                          _array_choice_days.add(index);
                        });
                      }else{
                        setState((){
                          _array_choice_days.removeWhere((element) => element==index);
                          if(_list_horarios.length > 0){
                            _list_horarios.removeWhere((element) => element['day']==index);
                          }
                        });
                      }
                    },
                    fillColor: Color.fromRGBO(151, 126, 242, 0.6),
                  ),
                ),
                Stack(
                  children: [
                    _array_choice_days.length>0?Container(
                      margin: EdgeInsets.fromLTRB(0,20.0,0,0),
                      child: ListView.separated(
                          scrollDirection: Axis.vertical,
                          shrinkWrap: true,
                          padding: EdgeInsets.fromLTRB(10.0,10.0,10.0,0),
                          itemCount: _array_choice_days.length,
                          itemBuilder:(context, index){
                            return Container(
                                margin: EdgeInsets.fromLTRB(0, 1, 0, 1),
                                padding: EdgeInsets.fromLTRB(0, 2, 0, 2),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10.0),
                                  boxShadow: [
                                    BoxShadow(
                                      blurRadius: 10.0,
                                      color: Colors.grey[200],
                                    )
                                  ],
                                ),
                                child:ListTile(
                                    title: NormalText(_days[_array_choice_days[index]],Color(0xff000000), 18),
                                    trailing: _list_horarios.any((element) => element['day']==_array_choice_days[index])?Text('[${_list_horarios.where((element) => element['day']==_array_choice_days[index]).toList()[0]['open']}-${_list_horarios.where((element) => element['day']==_array_choice_days[index]).toList()[0]['close']}]'):Icon(Icons.access_time),
                                    onTap: (){
                                      first_hour(context, _array_choice_days[index]);
                                    }
                                )
                            );
                          },
                          separatorBuilder: (context, index){
                            return Divider();
                          }
                      ),
                    ):Container()
                  ],
                )
              ],
            )
        ),
    );
  }

}