import 'dart:convert';
import 'dart:io';
import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:whereappv2/src/utils/TextStyles.dart';
import 'package:badges/badges.dart';
import 'package:whereappv2/src/utils/profile_user_default.dart';
import 'package:http/http.dart' as http;
import 'package:smart_select/smart_select.dart';




class addProducts extends StatefulWidget{
  String user_id;
  String bussines_id;

  addProducts(this.user_id, this.bussines_id);

  @override
  addProductsState createState() => addProductsState();
}

class addProductsState extends State<addProducts>{

  CollectionReference business = FirebaseFirestore.instance.collection('business');
  final GlobalKey<ScaffoldState> _scaffold = GlobalKey<ScaffoldState>();



  TextEditingController txtProduct = TextEditingController();
  TextEditingController txtp1 = TextEditingController();
  TextEditingController txtUrlImg = TextEditingController();
  TextEditingController txtId = TextEditingController();
  TextEditingController txtAPI = TextEditingController();

  File JsonFile;
  bool _selectedJson = false;

  int _contSuccess = 0;
  int _contErrors = 0;
  int _contSuccessApi = 0;
  int _contErrorsApi = 0;
  List<dynamic> _JsonFromFile = new List<dynamic>();
  List<Map<String,dynamic>> _listCategory = new List<Map<String,dynamic>>();

  bool _successUploadJson = false;
  String _categorySelected = '';




  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initializeDefault();
    getCategories();
  }

  Future<void> initializeDefault() async {
    FirebaseApp app = await Firebase.initializeApp();
    assert(app != null);
    print('Initialized default app $app');
  }

  getCategories(){
    FirebaseFirestore.instance
        .collection('category_products')
        .get()
        .then((QuerySnapshot querySnapshot) => {
          setState(() {
            _listCategory.clear();
          }),
          querySnapshot.docs.forEach((doc) {
            setState(() {
              _listCategory.add({'value':doc['ES'], 'title':doc['ES']});
            });
          })
    });
  }

  void snackMsj(String msj, Color color){//funcion que muestra mensajes snack, aquello que se muestran en la parte inferior del dispositivo
    _scaffold.currentState.showSnackBar(SnackBar(content: NormalText(msj, Colors.white, 16),
        backgroundColor:color));
  }



  Widget entryIdProduct() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText("ID: ",Colors.black, 16),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: txtId,
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.next,
            //maxLines: 3,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                fillColor: Color(0xfff3f3f4),
                filled: true,
                prefixIcon: Icon(FontAwesomeIcons.diceOne)
            ),)
        ],
      ),
    );
  }

  Widget entryProduct() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText("Producto: ",Colors.black, 16),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: txtProduct,
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.next,
            //maxLines: 3,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              fillColor: Color(0xfff3f3f4),
              filled: true,
              prefixIcon: Icon(FontAwesomeIcons.archive)
            ),)
        ],
      ),
    );
  }

  Widget entryPrice1() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText("Precio: ",Colors.black, 16),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: txtp1,
            keyboardType: TextInputType.numberWithOptions(),
            textInputAction: TextInputAction.next,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
              fillColor: Color(0xfff3f3f4),
              filled: true,
              prefixIcon: Icon(FontAwesomeIcons.moneyBill)
            ),)
        ],
      ),
    );
  }

  Widget entryUrlImg() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          NormalText("Imagen (URL):",Colors.black, 16),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: txtUrlImg,
            keyboardType: TextInputType.url,
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                fillColor: Color(0xfff3f3f4),
                filled: true,
                prefixIcon: Icon(FontAwesomeIcons.code)
            ),)
        ],
      ),
    );
  }


  Widget dropDown() {
    return Container(
      decoration:  BoxDecoration(
          border: Border.all(color: Colors.black)
      ),
      child: SmartSelect<String>.single(
          title: 'Categorias',
          placeholder: 'Escoger...',
          value: _categorySelected,
          onChange: (state) => setState(() => _categorySelected = state.value),
          choiceItems: S2Choice.listFrom<String, Map>(
            source: _listCategory,
            value: (index, item) => item['value'],
            title: (index, item) => item['title'],
          ),
          choiceGrouped: false,
          modalFilter: true,
          modalFilterAuto: true,
          modalFilterHint: 'Buscar...',
          tileBuilder: (context, state) {
            return S2Tile.fromState(
              state,
              isTwoLine: true,
            );
          }
      ),
    );
  }

  Future<void> addProduct() {
    return business
        .doc(widget.bussines_id)
        .collection('products_bussines')
        .doc(txtId.text.trim())
        .set({
          'product': txtProduct.text.trim(),
          'price': txtp1.text.trim(),
          'category': _categorySelected,
          'img':txtUrlImg.text.trim()
        })
        .then((value) => {
            snackMsj('Datos guardados correctamente', Colors.green),
           setState((){
             _categorySelected = '';
           }),
          txtp1.text = '',
          txtUrlImg.text = '',
          txtProduct.text = '',
          txtId.text = ''
        })
        .catchError((error) => print("Failed to add user: $error"));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffold,
        appBar: AppBar(
          title: NormalText("Agregar productos", Colors.black, 18),
          centerTitle: true,
          leading: IconButton(icon: Icon(Icons.close), color:Colors.black , onPressed: (){
            Navigator.pop(context);
          },),
          backgroundColor: Colors.white,
          actions: [
            IconButton(icon: Icon(Icons.file_upload), color:Colors.black ,tooltip: 'Carga masiva (JSON)',onPressed: (){
              if(_categorySelected != ''){
                showSheetActionsLoadJson(context);
              }else{
                snackMsj('Se debe seleccionar una categoria', Colors.red);
              }
            },),
            IconButton(icon: Icon(Icons.check), color:Colors.black ,onPressed: (){
              if(txtProduct.text != '' && txtp1.text != ''  && txtUrlImg.text != '' && txtId.text != '' && _categorySelected != ''){
                addProduct();
              }else{
                snackMsj('Todos los campos son obligatorios', Colors.red);
              }
            },)
          ],
        ),
        body: Container(
            margin: EdgeInsets.fromLTRB(15,15,15,5),
            padding: EdgeInsets.fromLTRB(1,1,1,1),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  dropDown(),
                  SizedBox(height: 10,
                    child: Divider(),
                  ),
                  entryIdProduct(),
                  entryProduct(),
                  entryPrice1(),
                  entryUrlImg()
                ],
              ),
            )
        )
    );
  }


  showSheetActionsLoadJson(BuildContext context){
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(5.0))),
        context: context,
        isScrollControlled: true,
        isDismissible: false,
        enableDrag:false,
        builder: (context) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return SingleChildScrollView(
                    child: Container(
                      padding:EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal:15, vertical: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 10.0),
                              child: NormalText('Cargar JSON (Via File o API URL)', Colors.black, 18),
                            ),
                            SizedBox(
                              height: 15,
                              child: Divider(),
                            ),
                            Container(
                              padding: const EdgeInsets.symmetric(horizontal: 10.0),
                              decoration:  BoxDecoration(
                                  border: Border.all(color: Colors.grey)
                              ),
                              child: Column(
                                //direction: Axis.vertical,
                                children: [
                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment:CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Expanded(
                                          flex: 1,
                                          child: NormalText(_selectedJson?'OK':'Seleccionar...', Colors.black, 16),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: FlatButton(child: Icon(Icons.attach_file), color: Color.fromRGBO(151, 126, 242, 0.3),onPressed: (){
                                            _pickJson(setState);
                                          }, ),
                                        )
                                      ]
                                  ),
                                  SizedBox(height: 15,
                                    child: Divider(),
                                  ),
                                  Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment:CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Expanded(
                                          flex: 1,
                                          child: Container(
                                            child: Badge(
                                              animationType: BadgeAnimationType.scale,
                                              badgeColor: Colors.green,
                                              badgeContent: Text(_contSuccess.toString(), style: TextStyle(color: Colors.white),),
                                              child: Icon(Icons.check),
                                            ),
                                          )
                                        ),

                                        Expanded(
                                          flex: 1,
                                          child: Badge(
                                            animationType: BadgeAnimationType.scale,
                                            badgeContent: Text(_contErrors.toString(), style: TextStyle(color: Colors.white)),
                                            child: Icon(Icons.error),
                                          ),
                                        )
                                      ]
                                  ),
                                ],
                              )
                            ),

                            SizedBox(
                              height: 25,
                              child: Divider(),
                            ),
                            Container(
                              child: TextField(
                                autocorrect: false,
                                controller: txtAPI,
                                decoration: InputDecoration(
                                    hintText: 'Ingresar url (API)',
                                    border: OutlineInputBorder(),
                                    fillColor: Color(0xfff3f3f4),
                                    filled: true,
                                    suffixIcon: IconButton(icon: Icon(Icons.code),onPressed: (){
                                      print("API");
                                    },)
                                ),
                              ),
                            ),
                            SizedBox(height: 10
                            ),
                            Container(
                                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment:CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Expanded(
                                        flex: 1,
                                        child: Container(
                                          child: Badge(
                                            animationType: BadgeAnimationType.scale,
                                            badgeColor: Colors.green,
                                            badgeContent: Text(_contSuccessApi.toString(), style: TextStyle(color: Colors.white),),
                                            child: Icon(Icons.check),
                                          ),
                                        )
                                    ),

                                    Expanded(
                                      flex: 1,
                                      child: Badge(
                                        animationType: BadgeAnimationType.scale,
                                        badgeContent: Text(_contErrorsApi.toString(), style: TextStyle(color: Colors.white)),
                                        child: Icon(Icons.error),
                                      ),
                                    )
                                  ]
                              ),
                            ),
                            SizedBox(height: 25,
                              child: Divider(),
                            ),
                            Container(
                              child: Row(
                                children: [
                                  FlatButton(onPressed: (){
                                      if(_successUploadJson){
                                        Navigator.pop(context);
                                        setState((){
                                          _successUploadJson = false;
                                          _selectedJson = false;
                                          _contErrors = 0;
                                          _contSuccess = 0;
                                          _contSuccessApi = 0;
                                          _contErrorsApi = 0;
                                        });
                                        _JsonFromFile.clear();
                                        txtAPI.text = '';
                                      }else{
                                        if(!(txtAPI.text != '') && _selectedJson){
                                          if(_JsonFromFile.length > 0){
                                            addProductFromFile(setState);
                                          }
                                        }else if(txtAPI.text != '' && !_selectedJson){
                                          loadApi(setState);
                                        }
                                      }
                                  }, child: !_successUploadJson?Icon(Icons.file_upload):Icon(Icons.check_circle_outline),  color: Color.fromRGBO(151, 126, 242, 0.7)),
                                  Container(margin: EdgeInsets.symmetric(horizontal: 2.0)),
                                  FlatButton(onPressed: (){
                                    setState((){
                                      _selectedJson = false;
                                    });
                                    Navigator.pop(context);
                                  },  child: Icon(Icons.close), color: Colors.red[300])
                                ],
                              ),
                            ),
                            SizedBox(height: 5),
                          ],
                        ),
                      ),
                    )
                );
              });
        });
  }

  loadApi(_state)async{
    final response = await http.get('https://bitbucket.org/dramoscuello/finvy/raw/a2428bb6fda497da374e6fb1071f7f64fa2d7dd3/resources/API/data.json');
    List<dynamic> respuesta;
    if(response.statusCode == 200){
      respuesta = json.decode(response.body);
      for(var obj in respuesta){
        await SaveProducts(obj, _state, 1);
      }
      _state((){
        _successUploadJson = true;
      });
    }else{
     snackMsj("Error recopilando información", Colors.red);
    }
  }

  Future<Null> _pickJson(_state) async {
    JsonFile = await FilePicker.getFile(type: FileType.any);
    if (JsonFile != null) {
      _JsonFromFile = json.decode(await new File(JsonFile.path).readAsString());
      _state(() {
        _selectedJson = true;
      });
    }
  }

  addProductFromFile(_state) async {
    for(var obj in _JsonFromFile){
      await SaveProducts(obj, _state, 0);
    }
    _state((){
      _successUploadJson = true;
    });
  }



  Future<void> SaveProducts(map, _state, opt) {
    return business
        .doc(widget.bussines_id)
        .collection('products_bussines')
        .doc(map['id'])
        .set({
      'product': map['product'],
      'price': map['price'],
      'category': _categorySelected,
      'img':map['img']!=null?map['img']:pic_default_product
    }).then((value) => {
          _state((){
            if(opt == 0){
              _contSuccess++;
            }else{
              _contSuccessApi++;
            }

          })
    }).catchError((error) =>{
          print("Failed to add user: $error"),
          _state((){
            if(opt==0){
              _contErrors++;
            }else{
              _contErrorsApi++;
            }
          })
        });
  }


}






